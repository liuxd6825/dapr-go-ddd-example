package handler

import (
	"context"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/user/event"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/user/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"testing"
)

func TestUserQueryHandler(t *testing.T) {
	test.InitQuery()

	queryHandler := NewUserQueryHandler()
	ctx := context.Background()

    id := randomutils.UUID()
    tenantId := test.TenantId

	t.Run("OnUserDeleteEventV1s0", func(t *testing.T) {
		e := event.NewUserDeleteEvent(randomutils.UUID())
		e.Data = field.UserDeleteFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnUserDeleteEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnUserCreateEventV1s0", func(t *testing.T) {
		e := event.NewUserCreateEvent(randomutils.UUID())
		e.Data = field.UserCreateFields{
            CaseId : "CaseId",
            Email : randomutils.String(10),
            Id: id,
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnUserCreateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnUserUpdateEventV1s0", func(t *testing.T) {
		e := event.NewUserUpdateEvent(randomutils.UUID())
		e.Data = field.UserUpdateFields{
            CaseId : "CaseId",
            Email : randomutils.String(10),
            Id: id,
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnUserUpdateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})
}
