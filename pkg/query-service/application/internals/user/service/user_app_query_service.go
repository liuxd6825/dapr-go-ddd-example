package service

import (
    "sync"
	"context"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/user/view"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/user/query"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/application/internals/user/appquery"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/application/internals/user/assembler"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/application/internals/user/executor"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/application/internals/user/executor/user_impl"
)

//
// UserAppQueryService
// @Description: <no value>查询应用服务类
//
type UserAppQueryService struct {
	userCreateExecutor     executor.UserCreateExecutor
	userCreateManyExecutor executor.UserCreateManyExecutor

	userUpdateExecutor     executor.UserUpdateExecutor
	userUpdateManyExecutor executor.UserUpdateManyExecutor

	userDeleteByIdExecutor executor.UserDeleteByIdExecutor
	userDeleteManyExecutor executor.UserDeleteManyExecutor
	userDeleteAllExecutor  executor.UserDeleteAllExecutor

    userFindAllExecutor    executor.UserFindAllExecutor
    userFindByIdExecutor   executor.UserFindByIdExecutor
    userFindByIdsExecutor  executor.UserFindByIdsExecutor
    userFindPagingExecutor executor.UserFindPagingExecutor
}

// 单例应用服务
var userAppQueryService *UserAppQueryService

// 并发安全
var onceUser sync.Once

//
// GetUserAppQueryService
// @Description: 获取单例应用服务
// @return *UserAppQueryService
//
func GetUserAppQueryService() *UserAppQueryService {
	onceUser.Do(func() {
		userAppQueryService = newUserAppQueryService()
	})
	return userAppQueryService
}

//
// NewUserAppQueryService
// @Description: 创建User查询应用服务
// @return *UserAppQueryService
//
func newUserAppQueryService() *UserAppQueryService {
	return &UserAppQueryService{
        userCreateExecutor:     user_impl.GetUserCreateExecutor(),
        userCreateManyExecutor: user_impl.GetUserCreateManyExecutor(),

        userUpdateExecutor:     user_impl.GetUserUpdateExecutor(),
        userUpdateManyExecutor: user_impl.GetUserUpdateManyExecutor(),

        userDeleteByIdExecutor: user_impl.GetUserDeleteByIdExecutor(),
        userDeleteManyExecutor: user_impl.GetUserDeleteManyExecutor(),
        userDeleteAllExecutor:  user_impl.GetUserDeleteAllExecutor(),

		userFindAllExecutor:    user_impl.GetUserFindAllExecutor(),
		userFindByIdExecutor:   user_impl.GetUserFindByIdExecutor(),
		userFindByIdsExecutor:  user_impl.GetUserFindByIdsExecutor(),
		userFindPagingExecutor: user_impl.GetUserFindPagingExecutor(),
	}
}

//
// Create
// @Description: 创建UserView
// @param ctx 上下文
// @param *view.UserView User实体对象
// @return error 错误
//
func (a *UserAppQueryService) Create(ctx context.Context, v *view.UserView) error {
	return a.userCreateExecutor.Execute(ctx, v)
}

//
// CreateMany
// @Description: 创建UserView
// @param ctx
// @return []*view.UserView  User实体对象切片
// @return error 错误
//
func (a *UserAppQueryService) CreateMany(ctx context.Context, vList []*view.UserView) error {
	return a.userCreateManyExecutor.Execute(ctx, vList)
}

//
// Update
// @Description: 按ID更新UserView
// @receiver a
// @param ctx
// @param v  *view.UserView
// @return error 错误
//
func (a *UserAppQueryService) Update(ctx context.Context, v *view.UserView) error {
	return a.userUpdateExecutor.Execute(ctx, v)
}

//
// UpdateMany
// @Description:  创建UserView
// @param ctx
// @return []*view.UserView  User实体对象切片
// @return error 错误
//
func (a *UserAppQueryService) UpdateMany(ctx context.Context, vList []*view.UserView) error {
	return a.userUpdateManyExecutor.Execute(ctx, vList)
}

//
// DeleteById
// @Description: 按ID删除UserView
// @param ctx
// @param tenantId 租户ID
// @param id 视图ID
// @return error 错误
//
func (a *UserAppQueryService) DeleteById(ctx context.Context, tenantId, id string) error {
	return a.userDeleteByIdExecutor.Execute(ctx, tenantId, id)
}

//
// DeleteMany
// @Description: 删除多个UserView
// @param ctx
// @param tenantId string 租户ID
// @param []*view.UserView  User视图切片
// @return error 错误
//
func (a *UserAppQueryService) DeleteMany(ctx context.Context, tenantId string, vList []*view.UserView) error {
	return a.userDeleteManyExecutor.Execute(ctx, tenantId, vList)
}

//
// DeleteAll
// @Description: 删除所有
// @receiver a
// @param ctx
// @param tenantId 租户ID
// @return error
//
func (a *UserAppQueryService) DeleteAll(ctx context.Context, tenantId string) error {
	return a.userDeleteAllExecutor.Execute(ctx, tenantId)
}

//
// FindById
// @Description: 按ID查询UserView
// @receiver a
// @param ctx
// @param qry 查询命令
// @return *view.UserView
// @return bool 是否查询到数据
// @return error
//
func (a *UserAppQueryService) FindById(ctx context.Context, tenantId string, id string) (*view.UserView, bool, error) {
	qry := assembler.User.AssFindByIdAppQuery(tenantId, id)
	return a.userFindByIdExecutor.Execute(ctx, qry)
}

//
// FindByIds
// @Description: 按多个ID查询UserView
// @receiver a
// @param ctx
// @param qry 查询命令
// @return *view.UserView
// @return bool 是否查询到数据
// @return error
//
func (a *UserAppQueryService) FindByIds(ctx context.Context, tenantId string, ids []string) ([]*view.UserView, bool, error) {
	qry := assembler.User.AssFindByIdsAppQuery(tenantId, ids)
	return a.userFindByIdsExecutor.Execute(ctx, qry)
}

//
// FindAll
// @Description: 查询所有view.UserView
// @receiver a
// @param ctx
// @param qry 查询命令
// @return []*view.UserView
// @return bool 是否查询到数据
// @return error 错误
//
func (a *UserAppQueryService) FindAll(ctx context.Context, tenantId string) ([]*view.UserView, bool, error) {
	qry := assembler.User.AssFindAllAppQuery(tenantId)
	return a.userFindAllExecutor.Execute(ctx, qry)
}

//
// FindPaging
// @Description: 分页查询
// @receiver a
// @param ctx 上下文
// @param qry 分页查询条件
// @return *appquery.UserFindPagingResult 分页数据
// @return bool 是否查询到数据
// @return error 错误
//
func (a *UserAppQueryService) FindPaging(ctx context.Context, aq *appquery.UserFindPagingAppQuery) (*appquery.UserFindPagingResult, bool, error) {
	return a.userFindPagingExecutor.Execute(ctx, aq)
}