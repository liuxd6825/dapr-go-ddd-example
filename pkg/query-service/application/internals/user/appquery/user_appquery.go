package appquery

import "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/user/view"

// UserFindByIdAppQuery 按id查询
type UserFindByIdAppQuery struct {
	TenantId string
	Id       string
}

// UserFindByIdsAppQuery 按多个id查询
type UserFindByIdsAppQuery struct {
	TenantId string
	Ids      []string
}

// UserFindPagingAppQuery 分页查询
type UserFindPagingAppQuery struct {
	TenantId string
	PageNum  int64
	PageSize int64
	Filter   string
	Sort     string
	Fields   string
}

// UserFindByAggregateIdAppQuery 按聚合根id查询
type UserFindByAggregateIdAppQuery struct {
	TenantId    string
	AggregateId string
}

// UserFindPagingResult 分页响应数据
type UserFindPagingResult struct {
	Data       []*view.UserView `json:"data"`
	TotalRows  *int64                `json:"totalRows,omitempty"`
	TotalPages *int64                `json:"totalPages,omitempty"`
	PageNum    int64                 `json:"pageNum"`
	PageSize   int64                 `json:"pageSize"`
	Filter     string                `json:"filter"`
	Sort       string                `json:"sort"`
	Error      error                 `json:"-"`
	IsFound    bool                  `json:"-"`
}

// UserFindAllAppQuery 查询所有数据
type UserFindAllAppQuery struct {
	TenantId string
}

func NewUserFindByIdAppQuery() *UserFindByIdAppQuery {
	return &UserFindByIdAppQuery{}
}

func NewUserFindByIdsAppQuery() *UserFindByIdsAppQuery {
	return &UserFindByIdsAppQuery{}
}

func NewUserFindByAggregateIdAppQuery() *UserFindByAggregateIdAppQuery {
	return &UserFindByAggregateIdAppQuery{}
}

func NewUserFindPagingAppQuery() *UserFindPagingAppQuery {
	return &UserFindPagingAppQuery{}
}

func NewUserFindPagingResult() *UserFindPagingResult {
	return &UserFindPagingResult{}
}

func NewUserFindAllAppQuery() *UserFindAllAppQuery {
	return &UserFindAllAppQuery{}
}
