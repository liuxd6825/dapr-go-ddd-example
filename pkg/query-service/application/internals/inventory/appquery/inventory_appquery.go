package appquery

import "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/inventory/view"

// InventoryFindByIdAppQuery 按id查询
type InventoryFindByIdAppQuery struct {
	TenantId string
	Id       string
}

// InventoryFindByIdsAppQuery 按多个id查询
type InventoryFindByIdsAppQuery struct {
	TenantId string
	Ids      []string
}

// InventoryFindPagingAppQuery 分页查询
type InventoryFindPagingAppQuery struct {
	TenantId string
	PageNum  int64
	PageSize int64
	Filter   string
	Sort     string
	Fields   string
}

// InventoryFindByAggregateIdAppQuery 按聚合根id查询
type InventoryFindByAggregateIdAppQuery struct {
	TenantId    string
	AggregateId string
}

// InventoryFindPagingResult 分页响应数据
type InventoryFindPagingResult struct {
	Data       []*view.InventoryView `json:"data"`
	TotalRows  *int64                `json:"totalRows,omitempty"`
	TotalPages *int64                `json:"totalPages,omitempty"`
	PageNum    int64                 `json:"pageNum"`
	PageSize   int64                 `json:"pageSize"`
	Filter     string                `json:"filter"`
	Sort       string                `json:"sort"`
	Error      error                 `json:"-"`
	IsFound    bool                  `json:"-"`
}

// InventoryFindAllAppQuery 查询所有数据
type InventoryFindAllAppQuery struct {
	TenantId string
}

func NewInventoryFindByIdAppQuery() *InventoryFindByIdAppQuery {
	return &InventoryFindByIdAppQuery{}
}

func NewInventoryFindByIdsAppQuery() *InventoryFindByIdsAppQuery {
	return &InventoryFindByIdsAppQuery{}
}

func NewInventoryFindByAggregateIdAppQuery() *InventoryFindByAggregateIdAppQuery {
	return &InventoryFindByAggregateIdAppQuery{}
}

func NewInventoryFindPagingAppQuery() *InventoryFindPagingAppQuery {
	return &InventoryFindPagingAppQuery{}
}

func NewInventoryFindPagingResult() *InventoryFindPagingResult {
	return &InventoryFindPagingResult{}
}

func NewInventoryFindAllAppQuery() *InventoryFindAllAppQuery {
	return &InventoryFindAllAppQuery{}
}
