package service

import (
	"context"
	"github.com/liuxd6825/dapr-go-ddd-sdk/errors"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/application/internals/inventory/appquery"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/inventory/view"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/utils"
	"testing"
	"time"
)

func TestInventoryQueryService(t *testing.T) {
	test.InitQuery()

	service := GetInventoryAppQueryService()

	ctx := context.Background()
	tenantId := test.TenantId
	id := randomutils.UUID()
	t.Logf("id = %v", id)
	var vList []*view.InventoryView

	t.Run("Create", func(t *testing.T) {
		v := &view.InventoryView{}
        v.Brand = randomutils.String(10)
        v.CaseId = "CaseId"
        v.CreatedTime = randomutils.PTime()
        v.CreatorId = "CreatorId"
        v.CreatorName = randomutils.NameCN()
        v.DeletedTime = randomutils.PTime()
        v.DeleterId = "DeleterId"
        v.DeleterName = randomutils.NameCN()
        v.Id = id
        v.IsDeleted = randomutils.Boolean()
        v.Keywords = randomutils.String(10)
        v.Name = randomutils.NameCN()
        v.Remarks = randomutils.String(10)
        v.Spec = randomutils.String(10)
        v.TenantId = tenantId
        v.UpdatedTime = randomutils.PTime()
        v.UpdaterId = "UpdaterId"
        v.UpdaterName = randomutils.NameCN()
        v.Labels = []string{"Inventory"}

		if err := utils.SetViewDefaultFields(ctx, v, time.Now(), utils.SetViewCreated); err != nil {
			t.Error(err)
		}
		if err := test.PrintJson(t, "Create", v); err != nil {
			t.Error(err)
		}
		if err := service.Create(ctx, v); err != nil {
			t.Error(err)
		}
		if findView, ok ,err :=service.FindById(ctx, tenantId, id); err!=nil {
			t.Error(err)
		} else if ok {
			if err := findView.Equal(v); err!=nil {
			    t.Error(err)
			}
		}
	})

	t.Run("Update", func(t *testing.T) {
		v := &view.InventoryView{}
        v.Brand = randomutils.String(10)
        v.CaseId = "CaseId"
        v.CreatedTime = randomutils.PTime()
        v.CreatorId = "CreatorId"
        v.CreatorName = randomutils.NameCN()
        v.DeletedTime = randomutils.PTime()
        v.DeleterId = "DeleterId"
        v.DeleterName = randomutils.NameCN()
        v.Id = id
        v.IsDeleted = randomutils.Boolean()
        v.Keywords = randomutils.String(10)
        v.Name = randomutils.NameCN()
        v.Remarks = randomutils.String(10)
        v.Spec = randomutils.String(10)
        v.TenantId = tenantId
        v.UpdatedTime = randomutils.PTime()
        v.UpdaterId = "UpdaterId"
        v.UpdaterName = randomutils.NameCN()
        v.Labels = []string{"Inventory"}

		if err := utils.SetViewDefaultFields(ctx, v, time.Now(), utils.SetViewUpdated); err != nil {
			t.Error(err)
		}
		if err := test.PrintJson(t, "Update", v); err != nil {
			t.Error(err)
		}
		if err := service.Update(ctx, v); err != nil {
			t.Error(err)
		}

        if findView, ok ,err :=service.FindById(ctx, tenantId, id); err!=nil {
            t.Error(err)
        } else if ok {
            if err := findView.Equal(v); err!=nil {
                t.Error(err)
            }
        }
	})

	t.Run("CreateMany", func(t *testing.T) {
		dateTime := time.Now()
		for i := 0; i < 10; i++ {
			v := &view.InventoryView{}
            v.Brand = randomutils.String(10)
            v.CaseId = "CaseId"
            v.CreatedTime = randomutils.PTime()
            v.CreatorId = "CreatorId"
            v.CreatorName = randomutils.NameCN()
            v.DeletedTime = randomutils.PTime()
            v.DeleterId = "DeleterId"
            v.DeleterName = randomutils.NameCN()
            v.Id = id
            v.IsDeleted = randomutils.Boolean()
            v.Keywords = randomutils.String(10)
            v.Name = randomutils.NameCN()
            v.Remarks = randomutils.String(10)
            v.Spec = randomutils.String(10)
            v.TenantId = tenantId
            v.UpdatedTime = randomutils.PTime()
            v.UpdaterId = "UpdaterId"
            v.UpdaterName = randomutils.NameCN()
			v.Labels = []string{"Inventory"}
			if err := utils.SetViewDefaultFields(ctx, v, dateTime, utils.SetViewCreated); err != nil {
				t.Error(err)
			}
			vList = append(vList, v)
		}

		if err := test.PrintJson(t, "CreateMany", vList); err != nil {
			t.Error(err)
		}
		if err := service.CreateMany(ctx, vList); err != nil {
			t.Error(err)
		}
	})

	t.Run("UpdateMany", func(t *testing.T) {
		for _, v := range vList {
            v.Brand = randomutils.String(10)
            v.CaseId = "CaseId"
            v.CreatedTime = randomutils.PTime()
            v.CreatorId = "CreatorId"
            v.CreatorName = randomutils.NameCN()
            v.DeletedTime = randomutils.PTime()
            v.DeleterId = "DeleterId"
            v.DeleterName = randomutils.NameCN()
            v.Id = id
            v.IsDeleted = randomutils.Boolean()
            v.Keywords = randomutils.String(10)
            v.Name = randomutils.NameCN()
            v.Remarks = randomutils.String(10)
            v.Spec = randomutils.String(10)
            v.TenantId = tenantId
            v.UpdatedTime = randomutils.PTime()
            v.UpdaterId = "UpdaterId"
            v.UpdaterName = randomutils.NameCN()
            v.Labels = []string{"Inventory"}
			if err := utils.SetViewDefaultFields(ctx, v, time.Now(), utils.SetViewUpdated); err != nil {
				t.Error(err)
			}
		}
		if err := test.PrintJson(t, "UpdateMany", vList); err != nil {
			t.Error(err)
		}
		if err := service.CreateMany(ctx, vList); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindById", func(t *testing.T) {
		if v, ok, err := service.FindById(ctx, tenantId, id); err != nil {
			t.Error(err)
		} else if ok {
            if err := test.PrintJson(t, "FindById", v); err != nil {
                t.Error(err)
            }
		} else {
			t.Error(errors.New("not found"))
		}
	})

	t.Run("FindByIds", func(t *testing.T) {
		var ids []string
		for _, v := range vList {
			ids = append(ids, v.Id)
		}

		if list, ok, err := service.FindByIds(ctx, tenantId, ids); err != nil {
			t.Error(err)
		} else if ok {
			if err := test.PrintJson(t, "FindByIds", list); err != nil {
				t.Error(err)
			}
		} else {
			t.Error(errors.New("not found"))
		}
	})

	t.Run("FindPaging", func(t *testing.T) {
		aq := &appquery.InventoryFindPagingAppQuery{
			TenantId: tenantId,
			PageNum:  0,
			PageSize: 10,
			Filter:   "",
			Sort:     "",
			Fields:   "",
		}
		if pagingResult, ok, err := service.FindPaging(ctx, aq); err != nil {
			t.Error(err)
		} else if ok {
			if err := test.PrintJson(t, "FindPaging", pagingResult); err != nil {
				t.Error(err)
			}
		} else {
			t.Error(errors.New("not found"))
		}
	})

    t.Run("FindAll", func(t *testing.T) {
        if list, ok, err := service.FindAll(ctx, tenantId); err != nil {
            t.Error(err)
        } else if ok {
            if err := test.PrintJson(t, "FindAll", list); err != nil {
                t.Error(err)
            }
        } else {
            t.Error(errors.New("not found"))
        }
    })

	t.Run("DeleteById", func(t *testing.T) {
		t.Logf("id = %v", id)
		if err := service.DeleteById(ctx, tenantId, id); err != nil {
			t.Error(err)
		}

		if v, ok, err := service.FindById(ctx, tenantId, id); err != nil {
            t.Error(err)
        } else if ok {
            t.Error(errors.New("delete failed"))
            if err := test.PrintJson(t, "DeleteById", v); err != nil {
                t.Error(err)
            }
        }
	})

	t.Run("DeleteMany", func(t *testing.T) {
		dList := vList[0:2]
		vList = vList[3:]
		if err := test.PrintJson(t, "DeleteMany", dList); err != nil {
			t.Error(err)
		}
		if err := service.DeleteMany(ctx, tenantId, dList); err != nil {
			t.Error(err)
		}

        var ids []string
        for _, v := range dList {
            ids = append(ids, v.Id)
        }

        if list, ok, err := service.FindByIds(ctx, tenantId, ids); err != nil {
            t.Error(err)
        } else if ok {
            t.Error(errors.New("delete failed"))
			if err := test.PrintJson(t, "DeleteMany", list); err != nil {
				t.Error(err)
			}
        }
	})

	t.Run("DeleteAll", func(t *testing.T) {
		if err := service.DeleteAll(ctx, tenantId); err != nil {
			t.Error(err)
		}
        if list, ok, err := service.FindAll(ctx, tenantId); err != nil {
            t.Error(err)
        } else if ok {
            t.Error(errors.New("delete failed"))
            if err := test.PrintJson(t, "DeleteAll", list); err != nil {
                t.Error(err)
            }
        }
	})

}
