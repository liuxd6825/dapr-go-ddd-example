package handler

import (
	"context"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/event"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"testing"
)

func TestInventoryQueryHandler(t *testing.T) {
	test.InitQuery()

	queryHandler := NewInventoryQueryHandler()
	ctx := context.Background()

    id := randomutils.UUID()
    tenantId := test.TenantId

	t.Run("OnInventoryCreateEventV1s0", func(t *testing.T) {
		e := event.NewInventoryCreateEvent(randomutils.UUID())
		e.Data = field.InventoryCreateFields{
            Brand : randomutils.String(10),
            CaseId : "CaseId",
            Id: id,
            Keywords : randomutils.String(10),
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            Spec : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnInventoryCreateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnInventoryUpdateEventV1s0", func(t *testing.T) {
		e := event.NewInventoryUpdateEvent(randomutils.UUID())
		e.Data = field.InventoryUpdateFields{
            Brand : randomutils.String(10),
            CaseId : "CaseId",
            Id: id,
            Keywords : randomutils.String(10),
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            Spec : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnInventoryUpdateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})
}
