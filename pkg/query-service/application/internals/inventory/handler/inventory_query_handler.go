package handler
import (
	"context"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/event"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/logs"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/inventory/view"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/inventory/factory"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/application/internals/inventory/service"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/base/application/handler"
	"github.com/liuxd6825/dapr-go-ddd-sdk/ddd"
	"github.com/liuxd6825/dapr-go-ddd-sdk/restapp"
)

type InventoryQueryHandler struct {
	service *service.InventoryAppQueryService
	handler.BaseQueryHandler
}
//
// NewInventorySubscribe
// @Description: 创建dapr消息订阅器，用于接受领域事件
// @return restapp.RegisterSubscribe  消息注册器
//
func NewInventorySubscribe() restapp.RegisterSubscribe {
	subscribes := &[]ddd.Subscribe{
		{PubsubName: "pubsub", Topic: event.InventoryCreateEventType.String(), Route: "/ddd-example/domain-event/inventory/inventory_create_event"},
		{PubsubName: "pubsub", Topic: event.InventoryUpdateEventType.String(), Route: "/ddd-example/domain-event/inventory/inventory_update_event"},
	}
	return restapp.NewRegisterSubscribe(subscribes, NewInventoryQueryHandler())
}

//
// NewInventoryQueryHandler
// @Description: 创建<no value>领域事件处理器
// @return ddd.QueryEventHandler 领域事件处理器
//
func NewInventoryQueryHandler() *InventoryQueryHandler{
	return &InventoryQueryHandler{
		service: service.GetInventoryAppQueryService(),
	}
}

//
// OnInventoryCreateEventV1s0
// @Description: InventoryCreateEvent事件处理器
// @receiver h
// @param ctx 上下文
// @param event InventoryCreateEvent 领域事件
// @return error 错误
//
func (h *InventoryQueryHandler) OnInventoryCreateEventV1s0(ctx context.Context, event *event.InventoryCreateEvent) error {
	logs.DebugEvent(event, "OnOnInventoryCreateEventV1s0")
	return h.DoSession(ctx, h, event, func(ctx context.Context) error {
        v, err := factory.InventoryView.NewByInventoryCreateEvent(ctx, event)
        if err != nil {
            return err
        }
        return h.service.Create(ctx, v)
	})
}

//
// OnInventoryUpdateEventV1s0
// @Description: InventoryUpdateEvent事件处理器
// @receiver h
// @param ctx 上下文
// @param event InventoryUpdateEvent 领域事件
// @return error 错误
//
func (h *InventoryQueryHandler) OnInventoryUpdateEventV1s0(ctx context.Context, event *event.InventoryUpdateEvent) error {
	logs.DebugEvent(event, "OnOnInventoryUpdateEventV1s0")
	return h.DoSession(ctx, h, event, func(ctx context.Context) error {
        v, err := factory.InventoryView.NewByInventoryUpdateEvent(ctx, event)
        if err != nil {
            return err
        }
        return h.service.Update(ctx, v)
	})
}

func (h *InventoryQueryHandler) GetStructName() string {
	return "ddd-example.inventory.InventoryQueryHandler"
}
