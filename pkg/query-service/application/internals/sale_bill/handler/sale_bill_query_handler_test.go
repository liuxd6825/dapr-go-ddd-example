package handler

import (
	"context"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/event"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"testing"
)

func TestSaleBillQueryHandler(t *testing.T) {
	test.InitQuery()

	queryHandler := NewSaleBillQueryHandler()
	ctx := context.Background()

    id := randomutils.UUID()
    tenantId := test.TenantId

	t.Run("OnSaleBillUpdateEventV1s0", func(t *testing.T) {
		e := event.NewSaleBillUpdateEvent(randomutils.UUID())
		e.Data = field.SaleBillUpdateFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            SaleMoney : randomutils.Float64(),
            SaleTime : randomutils.PTime(),
            TenantId : tenantId,
            UserId : "UserId",
            UserName : randomutils.NameCN(),
		}
		if err := queryHandler.OnSaleBillUpdateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnSaleBillConfirmEventV1s0", func(t *testing.T) {
		e := event.NewSaleBillConfirmEvent(randomutils.UUID())
		e.Data = field.SaleBillConfirmFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnSaleBillConfirmEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnSaleBillDeleteEventV1s0", func(t *testing.T) {
		e := event.NewSaleBillDeleteEvent(randomutils.UUID())
		e.Data = field.SaleBillDeleteFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}
		if err := queryHandler.OnSaleBillDeleteEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnSaleBillCreateEventV1s0", func(t *testing.T) {
		e := event.NewSaleBillCreateEvent(randomutils.UUID())
		e.Data = field.SaleBillCreateFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            SaleMoney : randomutils.Float64(),
            SaleTime : randomutils.PTime(),
            TenantId : tenantId,
            UserId : "UserId",
            UserName : randomutils.NameCN(),
		}
		if err := queryHandler.OnSaleBillCreateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})
}
