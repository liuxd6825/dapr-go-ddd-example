package handler

import (
	"context"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/event"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"testing"
)

func TestSaleItemQueryHandler(t *testing.T) {
	test.InitQuery()

	queryHandler := NewSaleItemQueryHandler()
	ctx := context.Background()

    id := randomutils.UUID()
    tenantId := test.TenantId

	t.Run("OnSaleItemCreateEventV1s0", func(t *testing.T) {
		e := event.NewSaleItemCreateEvent(randomutils.UUID())
		e.Data = field.SaleItemCreateFields{
                Items:[]*field.SaleItemCreateItem{
                    &field.SaleItemCreateItem{
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                        Id:id,
                        InventoryId:"InventoryId",
                        Money:randomutils.Float64(),
                        InventoryName:randomutils.NameCN(),
                        Quantity:randomutils.Int64(),
                    },
                },
            SaleBillId : "SaleBillId",
            TenantId : tenantId,
		}
		if err := queryHandler.OnSaleItemCreateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnSaleItemUpdateEventV1s0", func(t *testing.T) {
		e := event.NewSaleItemUpdateEvent(randomutils.UUID())
		e.Data = field.SaleItemUpdateFields{
                Items:[]*field.SaleItemUpdateItem{
                    &field.SaleItemUpdateItem{
                        Quantity:randomutils.Int64(),
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Id:id,
                        InventoryId:"InventoryId",
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                        InventoryName:randomutils.NameCN(),
                        Money:randomutils.Float64(),
                    },
                },
            SaleBillId : "SaleBillId",
            TenantId : tenantId,
		}
		if err := queryHandler.OnSaleItemUpdateEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})

	t.Run("OnSaleItemDeleteEventV1s0", func(t *testing.T) {
		e := event.NewSaleItemDeleteEvent(randomutils.UUID())
		e.Data = field.SaleItemDeleteFields{
                Items:[]*field.SaleItemDeleteItem{
                    &field.SaleItemDeleteItem{
                        Id:id,
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                    },
                },
            SaleBillId : "SaleBillId",
            TenantId : tenantId,
		}
		if err := queryHandler.OnSaleItemDeleteEventV1s0(ctx, e); err != nil {
			t.Error(err)
		}
	})
}
