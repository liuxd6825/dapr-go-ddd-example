package appquery

import "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/sale_bill/view"

// SaleItemFindByIdAppQuery 按id查询
type SaleItemFindByIdAppQuery struct {
	TenantId string
	Id       string
}

// SaleItemFindByIdsAppQuery 按多个id查询
type SaleItemFindByIdsAppQuery struct {
	TenantId string
	Ids      []string
}

// SaleItemFindPagingAppQuery 分页查询
type SaleItemFindPagingAppQuery struct {
	TenantId string
	PageNum  int64
	PageSize int64
	Filter   string
	Sort     string
	Fields   string
}

// SaleItemFindByAggregateIdAppQuery 按聚合根id查询
type SaleItemFindByAggregateIdAppQuery struct {
	TenantId    string
	AggregateId string
}

// SaleItemFindPagingResult 分页响应数据
type SaleItemFindPagingResult struct {
	Data       []*view.SaleItemView `json:"data"`
	TotalRows  *int64                `json:"totalRows,omitempty"`
	TotalPages *int64                `json:"totalPages,omitempty"`
	PageNum    int64                 `json:"pageNum"`
	PageSize   int64                 `json:"pageSize"`
	Filter     string                `json:"filter"`
	Sort       string                `json:"sort"`
	Error      error                 `json:"-"`
	IsFound    bool                  `json:"-"`
}

// SaleItemFindAllAppQuery 查询所有数据
type SaleItemFindAllAppQuery struct {
	TenantId string
}
// SaleItemFindBySaleBillIdAppQuery 按SaleBillId查询
type SaleItemFindBySaleBillIdAppQuery struct {
	TenantId string
	SaleBillId string
}

func NewSaleItemFindByIdAppQuery() *SaleItemFindByIdAppQuery {
	return &SaleItemFindByIdAppQuery{}
}

func NewSaleItemFindByIdsAppQuery() *SaleItemFindByIdsAppQuery {
	return &SaleItemFindByIdsAppQuery{}
}

func NewSaleItemFindByAggregateIdAppQuery() *SaleItemFindByAggregateIdAppQuery {
	return &SaleItemFindByAggregateIdAppQuery{}
}

func NewSaleItemFindPagingAppQuery() *SaleItemFindPagingAppQuery {
	return &SaleItemFindPagingAppQuery{}
}

func NewSaleItemFindPagingResult() *SaleItemFindPagingResult {
	return &SaleItemFindPagingResult{}
}

func NewSaleItemFindAllAppQuery() *SaleItemFindAllAppQuery {
	return &SaleItemFindAllAppQuery{}
}
func NewSaleItemFindBySaleBillIdAppQuery() *SaleItemFindBySaleBillIdAppQuery {
	return &SaleItemFindBySaleBillIdAppQuery{}
}
