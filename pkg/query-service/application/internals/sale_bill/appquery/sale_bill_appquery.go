package appquery

import "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/sale_bill/view"

// SaleBillFindByIdAppQuery 按id查询
type SaleBillFindByIdAppQuery struct {
	TenantId string
	Id       string
}

// SaleBillFindByIdsAppQuery 按多个id查询
type SaleBillFindByIdsAppQuery struct {
	TenantId string
	Ids      []string
}

// SaleBillFindPagingAppQuery 分页查询
type SaleBillFindPagingAppQuery struct {
	TenantId string
	PageNum  int64
	PageSize int64
	Filter   string
	Sort     string
	Fields   string
}

// SaleBillFindByAggregateIdAppQuery 按聚合根id查询
type SaleBillFindByAggregateIdAppQuery struct {
	TenantId    string
	AggregateId string
}

// SaleBillFindPagingResult 分页响应数据
type SaleBillFindPagingResult struct {
	Data       []*view.SaleBillView `json:"data"`
	TotalRows  *int64                `json:"totalRows,omitempty"`
	TotalPages *int64                `json:"totalPages,omitempty"`
	PageNum    int64                 `json:"pageNum"`
	PageSize   int64                 `json:"pageSize"`
	Filter     string                `json:"filter"`
	Sort       string                `json:"sort"`
	Error      error                 `json:"-"`
	IsFound    bool                  `json:"-"`
}

// SaleBillFindAllAppQuery 查询所有数据
type SaleBillFindAllAppQuery struct {
	TenantId string
}

func NewSaleBillFindByIdAppQuery() *SaleBillFindByIdAppQuery {
	return &SaleBillFindByIdAppQuery{}
}

func NewSaleBillFindByIdsAppQuery() *SaleBillFindByIdsAppQuery {
	return &SaleBillFindByIdsAppQuery{}
}

func NewSaleBillFindByAggregateIdAppQuery() *SaleBillFindByAggregateIdAppQuery {
	return &SaleBillFindByAggregateIdAppQuery{}
}

func NewSaleBillFindPagingAppQuery() *SaleBillFindPagingAppQuery {
	return &SaleBillFindPagingAppQuery{}
}

func NewSaleBillFindPagingResult() *SaleBillFindPagingResult {
	return &SaleBillFindPagingResult{}
}

func NewSaleBillFindAllAppQuery() *SaleBillFindAllAppQuery {
	return &SaleBillFindAllAppQuery{}
}
