
package factory

import (
	"context"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/event"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/utils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/sale_bill/view"
	"github.com/liuxd6825/dapr-go-ddd-sdk/errors"
)

type saleItemViewFactory struct {
}

var SaleItemView = &saleItemViewFactory{}

func (f *saleItemViewFactory) NewBySaleItemUpdateEvent (ctx context.Context, e *event.SaleItemUpdateEvent) ([]*view.SaleItemView, error) {
	if e == nil || len(e.Data.Items) == 0 {
		return []*view.SaleItemView{}, nil
	}

    var vList []*view.SaleItemView
    setViewType := utils.SetViewUpdated
    for _, item := range e.Data.Items {
        v := &view.SaleItemView{}
        v.CaseId = item.CaseId
        v.Id = item.Id
        v.InventoryId = item.InventoryId
        v.InventoryName = item.InventoryName
        v.Money = item.Money
        v.Quantity = item.Quantity
        v.Remarks = item.Remarks
        v.SaleBillId = item.SaleBillId
        v.TenantId = item.TenantId
        /*
        if err := utils.Mapper(item, v); err != nil {
            return nil, err
        }*/
        if err := utils.SetViewDefaultFields(ctx, v, e.GetCreatedTime(), setViewType); err != nil {
            return nil, err
        }
        vList = append(vList, v)
    }
    return vList, nil
}

func (f *saleItemViewFactory) NewBySaleItemDeleteEvent (ctx context.Context, e *event.SaleItemDeleteEvent) ([]*view.SaleItemView, error) {
	if e == nil || len(e.Data.Items) == 0 {
		return []*view.SaleItemView{}, nil
	}

    var vList []*view.SaleItemView
    setViewType := utils.SetViewDeleted
    for _, item := range e.Data.Items {
        v := &view.SaleItemView{}
        v.CaseId = item.CaseId
        v.Id = item.Id
        v.Remarks = item.Remarks
        v.SaleBillId = item.SaleBillId
        v.TenantId = item.TenantId
        /*
        if err := utils.Mapper(item, v); err != nil {
            return nil, err
        }*/
        if err := utils.SetViewDefaultFields(ctx, v, e.GetCreatedTime(), setViewType); err != nil {
            return nil, err
        }
        vList = append(vList, v)
    }
    return vList, nil
}

func (f *saleItemViewFactory) NewBySaleItemCreateEvent (ctx context.Context, e *event.SaleItemCreateEvent) ([]*view.SaleItemView, error) {
	if e == nil || len(e.Data.Items) == 0 {
		return []*view.SaleItemView{}, nil
	}

    var vList []*view.SaleItemView
    setViewType := utils.SetViewCreated
    for _, item := range e.Data.Items {
        v := &view.SaleItemView{}
        v.CaseId = item.CaseId
        v.Id = item.Id
        v.InventoryId = item.InventoryId
        v.InventoryName = item.InventoryName
        v.Money = item.Money
        v.Quantity = item.Quantity
        v.Remarks = item.Remarks
        v.SaleBillId = item.SaleBillId
        v.TenantId = item.TenantId
        /*
        if err := utils.Mapper(item, v); err != nil {
            return nil, err
        }*/
        if err := utils.SetViewDefaultFields(ctx, v, e.GetCreatedTime(), setViewType); err != nil {
            return nil, err
        }
        vList = append(vList, v)
    }
    return vList, nil
}