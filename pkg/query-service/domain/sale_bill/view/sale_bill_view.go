
package view

import (
    "time"
    base "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/base/domain/view"
)

//
// SaleBillView
// @Description: 销售订单
//
type SaleBillView struct {
    base.BaseNeo4jView `bson:",inline"`
    SaleMoney float64 `json:"saleMoney,omitempty" bson:"sale_money"  gorm:""`  // 销售金额
    SaleTime *time.Time `json:"saleTime,omitempty" bson:"sale_time"  gorm:""`  // 文件大小
    Statue SaleBillStatue `json:"statue,omitempty" bson:"statue"  gorm:""`  // 单据状态
    UserId string `json:"userId,omitempty" bson:"user_id"  gorm:"index:idx_user_id"`  // 用户Id
    UserName string `json:"userName,omitempty" bson:"user_name"  gorm:""`  // 用户名称
}


//
// NewSaleBillView
// @Description: 销售订单
//
func NewSaleBillView()*SaleBillView{
    return &SaleBillView{}
}

//
// Equal
// @Description:  对比销售订单
//
func (v *SaleBillView) Equal(v2 *SaleBillView) error {
	var msg []string
    if !reflect.DeepEqual(v.CaseId, v2.CaseId) {
        msg = append(msg, "CaseId")
    }
    if !reflect.DeepEqual(v.CreatedTime, v2.CreatedTime) {
        msg = append(msg, "CreatedTime")
    }
    if !reflect.DeepEqual(v.CreatorId, v2.CreatorId) {
        msg = append(msg, "CreatorId")
    }
    if !reflect.DeepEqual(v.CreatorName, v2.CreatorName) {
        msg = append(msg, "CreatorName")
    }
    if !reflect.DeepEqual(v.DeletedTime, v2.DeletedTime) {
        msg = append(msg, "DeletedTime")
    }
    if !reflect.DeepEqual(v.DeleterId, v2.DeleterId) {
        msg = append(msg, "DeleterId")
    }
    if !reflect.DeepEqual(v.DeleterName, v2.DeleterName) {
        msg = append(msg, "DeleterName")
    }
    if !reflect.DeepEqual(v.Id, v2.Id) {
        msg = append(msg, "Id")
    }
    if !reflect.DeepEqual(v.IsDeleted, v2.IsDeleted) {
        msg = append(msg, "IsDeleted")
    }
    if !reflect.DeepEqual(v.Remarks, v2.Remarks) {
        msg = append(msg, "Remarks")
    }
    if !reflect.DeepEqual(v.SaleItems, v2.SaleItems) {
        msg = append(msg, "SaleItems")
    }
    if !reflect.DeepEqual(v.SaleMoney, v2.SaleMoney) {
        msg = append(msg, "SaleMoney")
    }
    if !reflect.DeepEqual(v.SaleTime, v2.SaleTime) {
        msg = append(msg, "SaleTime")
    }
    if !reflect.DeepEqual(v.Statue, v2.Statue) {
        msg = append(msg, "Statue")
    }
    if !reflect.DeepEqual(v.TenantId, v2.TenantId) {
        msg = append(msg, "TenantId")
    }
    if !reflect.DeepEqual(v.UpdatedTime, v2.UpdatedTime) {
        msg = append(msg, "UpdatedTime")
    }
    if !reflect.DeepEqual(v.UpdaterId, v2.UpdaterId) {
        msg = append(msg, "UpdaterId")
    }
    if !reflect.DeepEqual(v.UpdaterName, v2.UpdaterName) {
        msg = append(msg, "UpdaterName")
    }
    if !reflect.DeepEqual(v.UserId, v2.UserId) {
        msg = append(msg, "UserId")
    }
    if !reflect.DeepEqual(v.UserName, v2.UserName) {
        msg = append(msg, "UserName")
    }
	var err error
	if len(msg) > 0{
	    err = errors.New(strings.Join(msg, ","))
	}
	return err
}

