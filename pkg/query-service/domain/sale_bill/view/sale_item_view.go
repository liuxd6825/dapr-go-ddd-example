
package view

import (
    
    base "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/base/domain/view"
)

//
// SaleItemView
// @Description: 销售明细项
//
type SaleItemView struct {
    base.BaseNeo4jView `bson:",inline"`
    InventoryId string `json:"inventoryId,omitempty"  bson:"inventory_id" gorm:"index:idx_inventory_id"`  // 存货Id
    InventoryName string `json:"inventoryName,omitempty"  bson:"inventory_name" gorm:""`  // 存货名称
    Money float64 `json:"money,omitempty"  bson:"money" gorm:""`  // 文件大小
    Quantity int64 `json:"quantity,omitempty"  bson:"quantity" gorm:""`  // 数量
    SaleBillId string `json:"saleBillId,omitempty"  bson:"sale_bill_id" validate:"gt=0" gorm:"index:idx_sale_bill_id"` 
}

//
// NewSaleItemView
// @Description: 创建 销售明细项 视图对象
//
func NewSaleItemView()*SaleItemView{
    return &SaleItemView{}
}

//
// Equal
// @Description:  对比销售明细项
//
func (v *SaleItemView) Equal(v2 *SaleItemView) error {
	var msg []string
    if !reflect.DeepEqual(v.CaseId, v2.CaseId) {
        msg = append(msg, "CaseId")
    }
    if !reflect.DeepEqual(v.CreatedTime, v2.CreatedTime) {
        msg = append(msg, "CreatedTime")
    }
    if !reflect.DeepEqual(v.CreatorId, v2.CreatorId) {
        msg = append(msg, "CreatorId")
    }
    if !reflect.DeepEqual(v.CreatorName, v2.CreatorName) {
        msg = append(msg, "CreatorName")
    }
    if !reflect.DeepEqual(v.DeletedTime, v2.DeletedTime) {
        msg = append(msg, "DeletedTime")
    }
    if !reflect.DeepEqual(v.DeleterId, v2.DeleterId) {
        msg = append(msg, "DeleterId")
    }
    if !reflect.DeepEqual(v.DeleterName, v2.DeleterName) {
        msg = append(msg, "DeleterName")
    }
    if !reflect.DeepEqual(v.Id, v2.Id) {
        msg = append(msg, "Id")
    }
    if !reflect.DeepEqual(v.IsDeleted, v2.IsDeleted) {
        msg = append(msg, "IsDeleted")
    }
    if !reflect.DeepEqual(v.Remarks, v2.Remarks) {
        msg = append(msg, "Remarks")
    }
    if !reflect.DeepEqual(v.SaleItems, v2.SaleItems) {
        msg = append(msg, "SaleItems")
    }
    if !reflect.DeepEqual(v.SaleMoney, v2.SaleMoney) {
        msg = append(msg, "SaleMoney")
    }
    if !reflect.DeepEqual(v.SaleTime, v2.SaleTime) {
        msg = append(msg, "SaleTime")
    }
    if !reflect.DeepEqual(v.Statue, v2.Statue) {
        msg = append(msg, "Statue")
    }
    if !reflect.DeepEqual(v.TenantId, v2.TenantId) {
        msg = append(msg, "TenantId")
    }
    if !reflect.DeepEqual(v.UpdatedTime, v2.UpdatedTime) {
        msg = append(msg, "UpdatedTime")
    }
    if !reflect.DeepEqual(v.UpdaterId, v2.UpdaterId) {
        msg = append(msg, "UpdaterId")
    }
    if !reflect.DeepEqual(v.UpdaterName, v2.UpdaterName) {
        msg = append(msg, "UpdaterName")
    }
    if !reflect.DeepEqual(v.UserId, v2.UserId) {
        msg = append(msg, "UserId")
    }
    if !reflect.DeepEqual(v.UserName, v2.UserName) {
        msg = append(msg, "UserName")
    }
	var err error
	if len(msg) > 0{
	    err = errors.New(strings.Join(msg, ","))
	}
	return err
}

