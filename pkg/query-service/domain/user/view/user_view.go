
package view

import (
    
    base "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/base/domain/view"
)

//
// UserView
// @Description: 用户
//
type UserView struct {
    base.BaseNeo4jView `bson:",inline"`
    Email string `json:"email,omitempty" bson:"email"  gorm:""`  // 电子邮箱
    Name string `json:"name,omitempty" bson:"name"  gorm:""`  // 用户名称
}


//
// NewUserView
// @Description: 用户
//
func NewUserView()*UserView{
    return &UserView{}
}

//
// Equal
// @Description:  对比用户
//
func (v *UserView) Equal(v2 *UserView) error {
	var msg []string
    if !reflect.DeepEqual(v.CaseId, v2.CaseId) {
        msg = append(msg, "CaseId")
    }
    if !reflect.DeepEqual(v.CreatedTime, v2.CreatedTime) {
        msg = append(msg, "CreatedTime")
    }
    if !reflect.DeepEqual(v.CreatorId, v2.CreatorId) {
        msg = append(msg, "CreatorId")
    }
    if !reflect.DeepEqual(v.CreatorName, v2.CreatorName) {
        msg = append(msg, "CreatorName")
    }
    if !reflect.DeepEqual(v.DeletedTime, v2.DeletedTime) {
        msg = append(msg, "DeletedTime")
    }
    if !reflect.DeepEqual(v.DeleterId, v2.DeleterId) {
        msg = append(msg, "DeleterId")
    }
    if !reflect.DeepEqual(v.DeleterName, v2.DeleterName) {
        msg = append(msg, "DeleterName")
    }
    if !reflect.DeepEqual(v.Email, v2.Email) {
        msg = append(msg, "Email")
    }
    if !reflect.DeepEqual(v.Id, v2.Id) {
        msg = append(msg, "Id")
    }
    if !reflect.DeepEqual(v.IsDeleted, v2.IsDeleted) {
        msg = append(msg, "IsDeleted")
    }
    if !reflect.DeepEqual(v.Name, v2.Name) {
        msg = append(msg, "Name")
    }
    if !reflect.DeepEqual(v.Remarks, v2.Remarks) {
        msg = append(msg, "Remarks")
    }
    if !reflect.DeepEqual(v.TenantId, v2.TenantId) {
        msg = append(msg, "TenantId")
    }
    if !reflect.DeepEqual(v.UpdatedTime, v2.UpdatedTime) {
        msg = append(msg, "UpdatedTime")
    }
    if !reflect.DeepEqual(v.UpdaterId, v2.UpdaterId) {
        msg = append(msg, "UpdaterId")
    }
    if !reflect.DeepEqual(v.UpdaterName, v2.UpdaterName) {
        msg = append(msg, "UpdaterName")
    }
	var err error
	if len(msg) > 0{
	    err = errors.New(strings.Join(msg, ","))
	}
	return err
}

