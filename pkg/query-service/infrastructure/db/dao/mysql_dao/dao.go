package mysql_dao

import (
	"context"
	"errors"
	"fmt"
	"github.com/liuxd6825/dapr-go-ddd-sdk/ddd"
	"github.com/liuxd6825/dapr-go-ddd-sdk/ddd/ddd_repository"
	"github.com/liuxd6825/dapr-go-ddd-sdk/rsql"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/stringutils"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gorm.io/gorm"
	"strings"
	"time"
)

type Dao[T ddd.Entity] struct {
	db          *gorm.DB
	newFunc     func() T
	newListFunc func() []T
}

type FindQuery struct {
	Where  string
	Limit  uint
	Sort   string
	Offset uint
}

type RsqlProcess interface {
	OnAndItem()
	OnAndStart()
	OnAndEnd()
	OnOrItem()
	OnOrStart()
	OnOrEnd()
	OnEquals(name string, value interface{}, rValue rsql.Value)
	OnNotEquals(name string, value interface{}, rValue rsql.Value)
	OnLike(name string, value interface{}, rValue rsql.Value)
	OnNotLike(name string, value interface{}, rValue rsql.Value)
	OnGreaterThan(name string, value interface{}, rValue rsql.Value)
	OnGreaterThanOrEquals(name string, value interface{}, rValue rsql.Value)
	OnLessThan(name string, value interface{}, rValue rsql.Value)
	OnLessThanOrEquals(name string, value interface{}, rValue rsql.Value)
	OnIn(name string, value interface{}, rValue rsql.Value)
	OnNotIn(name string, value interface{}, rValue rsql.Value)
	GetSqlWhere(tenantId string) interface{}
	GetFilter(tenantId string) interface{}
}

//
// Options
// @Description: Options DAO 选项
//
type Options struct {
	Db      *gorm.DB
	DbId    string
	MaxTime *time.Duration
	Sort    *string
}

//
// contextKey
// @Description: 事务上下文
//
type contextKey struct {
}

//
// rsqlProcess
// @Description: RSQL处理进程
//
type rsqlProcess struct {
	str string
}

//
//  session
//  @Description: 事务
//
type session struct {
}

var (
	_contextKey = &contextKey{}
	_db         *gorm.DB
)

const (
	WhereTenantId      = "tenant_id=?"
	WhereTenantIdAndId = "tenant_id=? and id=?"
)

func NewOptions() *Options {
	return &Options{}
}

func NewSqlProcess() RsqlProcess {
	return &rsqlProcess{}
}

func NewSession(isWrite bool) ddd_repository.Session {
	return &session{}
}

func NewContext(parentCtx context.Context, tx *gorm.DB) context.Context {
	return context.WithValue(parentCtx, _contextKey, tx)
}

func NewDao[T ddd.Entity](db *gorm.DB, newFunc func() T, newListFunc func() []T) *Dao[T] {
	dao := Dao[T]{
		db:          db,
		newFunc:     newFunc,
		newListFunc: newListFunc,
	}
	return &dao
}

func (d *Dao[T]) Insert(ctx context.Context, v T, opts ...ddd_repository.Options) error {
	if err := d.getDB(ctx).AutoMigrate(v); err != nil {
		return err
	}
	return d.getDB(ctx).Model(v).Create(v).Error
}

func (d *Dao[T]) InsertMany(ctx context.Context, vList []T, opts ...ddd_repository.Options) error {
	v := d.NewEntity()
	return d.getDB(ctx).Model(v).CreateInBatches(vList, len(vList)).Error
}

func (d *Dao[T]) Update(ctx context.Context, v T, opts ...ddd_repository.Options) error {
	return d.getDB(ctx).Where(WhereTenantIdAndId, v.GetTenantId(), v.GetId()).Updates(&v).Error
}

func (d *Dao[T]) UpdateMany(ctx context.Context, vList []T, opts ...ddd_repository.Options) error {
	getDB := d.getDB(ctx)
	var model T
	for _, item := range vList {
		model = d.NewEntity()
		model.SetId(item.GetId())
		model.SetTenantId(item.GetTenantId())
		err := getDB.Model(&model).Updates(item).Error
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *Dao[T]) UpdateManyByFilter(ctx context.Context, tenantId, filter string, data interface{}, opts ...ddd_repository.Options) error {
	where, err := getSqlWhere(tenantId, filter)
	if err != nil {
		return err
	}
	return d.getDB(ctx).Where(where).Updates(data).Error
}

func (d *Dao[T]) UpdateByMap(ctx context.Context, tenantId, id string, data map[string]interface{}, opts ...*Options) error {
	var v = d.NewEntity()
	return d.getDB(ctx).Model(v).Where(WhereTenantIdAndId, tenantId, id).Updates(data).Error
}

func (d *Dao[T]) DeleteById(ctx context.Context, tenantId string, id string, opts ...ddd_repository.Options) error {
	var v = d.NewEntity()
	v.SetId(id)
	v.SetTenantId(tenantId)
	return d.getDB(ctx).Where(WhereTenantId, tenantId).Delete(&v).Error
}

func (d *Dao[T]) DeleteByIds(ctx context.Context, tenantId string, ids []string, opts ...ddd_repository.Options) error {
	var models []T
	models = d.NewEntities()
	return d.getDB(ctx).Where(WhereTenantId, tenantId).Delete(&models, ids).Error
}

func (d *Dao[T]) DeleteAll(ctx context.Context, tenantId string, opts ...ddd_repository.Options) error {
	var model T
	model = d.NewEntity()
	return d.getDB(ctx).Where(WhereTenantId, tenantId).Delete(&model).Error
}

func (d *Dao[T]) DeleteByFilter(ctx context.Context, tenantId string, filter string, opts ...ddd_repository.Options) error {
	var model T
	model = d.NewEntity()
	where, err := getSqlWhere(tenantId, filter)
	if err != nil {
		return err
	}
	return d.getDB(ctx).Where(where).Delete(&model).Error
}

func (d *Dao[T]) FindById(ctx context.Context, tenantId string, id string, opts ...ddd_repository.Options) (T, bool, error) {
	var model T
	model = d.NewEntity()
	tx := d.getDB(ctx).Where(WhereTenantIdAndId, tenantId, id).Find(&model)
	if tx.Error == gorm.ErrRecordNotFound {
		return model, false, nil
	} else if tx.Error != nil {
		return model, false, tx.Error
	}
	return model, true, nil
}

func (d *Dao[T]) FindByIds(ctx context.Context, tenantId string, ids []string, opts ...ddd_repository.Options) ([]T, bool, error) {
	var vList []T
	vList = d.NewEntities()
	tx := d.getDB(ctx).Where(ids).Find(&vList)
	if IsErrRecordNotFound(tx.Error) {
		return vList, false, nil
	} else if tx.Error != nil {
		return vList, false, tx.Error
	}
	return vList, len(vList) > 0, tx.Error
}

func (d *Dao[T]) FindAll(ctx context.Context, tenantId string, opts ...ddd_repository.Options) *ddd_repository.FindListResult[T] {
	var vList []T
	vList = d.NewEntities()
	tx := d.getDB(ctx).Where(WhereTenantId, tenantId).Find(&vList)
	if IsErrRecordNotFound(tx.Error) {
		return ddd_repository.NewFindListResult(vList, false, nil)
	} else if tx.Error != nil {
		return ddd_repository.NewFindListResult(vList, false, tx.Error)
	}
	return ddd_repository.NewFindListResult(vList, len(vList) > 0, nil)
}

func (d *Dao[T]) FindListByMap(ctx context.Context, tenantId string, filterMap map[string]interface{}, opts ...ddd_repository.Options) *ddd_repository.FindListResult[T] {
	var vList []T
	vList = d.NewEntities()
	tx := d.getDB(ctx).Where(filterMap).Find(&vList)
	if IsErrRecordNotFound(tx.Error) {
		return ddd_repository.NewFindListResult(vList, false, nil)
	} else if tx.Error != nil {
		return ddd_repository.NewFindListResult(vList, false, tx.Error)
	}
	return ddd_repository.NewFindListResult(vList, len(vList) > 0, nil)
}

func (d *Dao[T]) FindPaging(ctx context.Context, query ddd_repository.FindPagingQuery, opts ...ddd_repository.Options) *ddd_repository.FindPagingResult[T] {
	return d.findPaging(ctx, query)
}

func (d *Dao[T]) findPaging(ctx context.Context, query ddd_repository.FindPagingQuery, opts ...ddd_repository.Options) *ddd_repository.FindPagingResult[T] {
	return d.DoFilter(query.GetTenantId(), query.GetFilter(), func(sqlWhere string) (*ddd_repository.FindPagingResult[T], bool, error) {
		var data = d.NewEntities()

		tx := d.getDB(ctx)

		if len(sqlWhere) > 0 {
			tx = tx.Where(sqlWhere)
		}

		if query.GetPageSize() > 0 {
			tx = tx.Limit(int(query.GetPageSize()))
		}

		if query.GetPageNum() > 0 {
			tx = tx.Offset(int(query.GetPageSize() * query.GetPageNum()))
		}

		if len(query.GetSort()) > 0 {
			order, err := d.getOrder(query.GetSort())
			if err != nil {
				return nil, false, err
			}
			tx = tx.Order(order)
		}

		err := tx.Find(&data).Error
		if IsErrRecordNotFound(err) {
			return nil, false, nil
		}

		var totalRows int64 = -1
		if query.GetIsTotalRows() {
			tx := d.getDB(ctx)
			if len(sqlWhere) > 0 {
				tx = tx.Where(sqlWhere)
			}
			if err := tx.Count(&totalRows).Error; err != nil {
				return nil, false, err
			}
		}

		findData := ddd_repository.NewFindPagingResult[T](data, totalRows, query, err)
		if len(data) > 0 {
			findData.IsFound = true
		}
		return findData, true, err
	})

}

func (d *Dao[T]) getDbByQuery(ctx context.Context, tenantId string, query *FindQuery) *gorm.DB {
	tx := d.getDB(ctx)
	if query != nil {
		tx = tx.Limit(int(query.Limit))
		tx = tx.Offset(int(query.Offset))
		tx = tx.Order(query.Sort)
		w := "tenant_id=?"
		if len(query.Where) > 0 {
			w = fmt.Sprintf("(%v) and tenant_id=?", query.Where)
		}
		tx = tx.Where(w, tenantId)
	} else {
		tx = tx.Where(WhereTenantId, tenantId)
	}
	return tx
}

func (d *Dao[T]) FindList(ctx context.Context, tenantId string, query *FindQuery, opts ...ddd_repository.Options) ([]T, bool, error) {
	list := d.NewEntities()
	tx := d.getDbByQuery(ctx, tenantId, query)
	err := tx.Find(&list).Error
	if IsErrRecordNotFound(err) {
		return list, false, nil
	} else if err != nil {
		return list, false, err
	}
	return list, true, nil
}

func (d *Dao[T]) DoFilter(tenantId, filter string, fun func(sqlWhere string) (*ddd_repository.FindPagingResult[T], bool, error), opts ...ddd_repository.Options) *ddd_repository.FindPagingResult[T] {
	p := NewSqlProcess()
	if err := ParseRsqlProcess(filter, p); err != nil {
		return ddd_repository.NewFindPagingResultWithError[T](err)
	}
	sqlWhere, err := getSqlWhere(tenantId, filter)
	if err != nil {
		return ddd_repository.NewFindPagingResultWithError[T](err)
	}
	data, _, err := fun(sqlWhere)
	if err != nil {
		if IsErrRecordNotFound(err) {
			err = nil
		}
	}
	return data
}

func (d *Dao[T]) getOrder(sort string) (string, error) {
	if len(sort) == 0 {
		return "", nil
	}
	//name:desc,id:asc
	strBuilder := strings.Builder{}
	list := strings.Split(sort, ",")
	for i := 0; i < len(list); i++ {
		str := list[i]
		sortItem := strings.Split(str, ":")
		name := sortItem[0]
		name = stringutils.AsFieldName(name)

		order := "asc"
		if len(sortItem) > 1 {
			order = sortItem[1]
			order = strings.ToLower(order)
			order = strings.Trim(order, " ")
		}

		strBuilder.WriteString(name)
		strBuilder.WriteString(" ")
		strBuilder.WriteString(order)
		if i < len(list)-1 {
			strBuilder.WriteString(",")
		}
	}
	return strBuilder.String(), nil
}

func (d *Dao[T]) getDB(ctx context.Context) *gorm.DB {
	tx := GetTransaction(ctx)
	if tx != nil {
		return tx
	}
	return GetDB()
}

func (d *Dao[T]) NewEntity() T {
	return d.newFunc()
}

func (d *Dao[T]) NewEntities() []T {
	return d.newListFunc()
}

func (p *rsqlProcess) OnNotEquals(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s != (%v)", p.str, name, value)
}

func (p *rsqlProcess) OnLike(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s like (%v)", p.str, name, value)
}

func (p *rsqlProcess) OnNotLike(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s not like %v", p.str, name, value)
}

func (p *rsqlProcess) OnGreaterThan(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s>%v", p.str, name, value)
}

func (p *rsqlProcess) OnGreaterThanOrEquals(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s>=%v", p.str, name, value)
}

func (p *rsqlProcess) OnLessThan(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s<%v", p.str, name, value)
}

func (p *rsqlProcess) OnLessThanOrEquals(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s <= %v", p.str, name, value)
}

func (p *rsqlProcess) OnIn(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s in %v", p.str, name, value)
}

func (p *rsqlProcess) OnNotIn(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s not in %v", p.str, name, value)
}

func (p *rsqlProcess) OnEquals(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s=%v", p.str, name, value)
}

func (p *rsqlProcess) NotEquals(name string, value interface{}, rValue rsql.Value) {
	p.str = fmt.Sprintf("%s %s=%v", p.str, name, value)
}

func (p *rsqlProcess) OnAndItem() {
	p.str = fmt.Sprintf("%s and ", p.str)
}
func (p *rsqlProcess) OnAndStart() {
	p.str = fmt.Sprintf("%s(", p.str)
}
func (p *rsqlProcess) OnAndEnd() {
	p.str = fmt.Sprintf("%s)", p.str)
}
func (p *rsqlProcess) OnOrItem() {
	p.str = fmt.Sprintf("%s or ", p.str)
}
func (p *rsqlProcess) OnOrStart() {
	p.str = fmt.Sprintf("%s(", p.str)
}
func (p *rsqlProcess) OnOrEnd() {
	p.str = fmt.Sprintf("%s)", p.str)
}

func (p *rsqlProcess) GetFilter(tenantId string) interface{} {
	return p.str
}

func (p *rsqlProcess) GetSqlWhere(tenantId string) interface{} {
	return p.str
}
func (p *rsqlProcess) Print() {
	fmt.Print(p.str)
}

func (o *Options) SetDB(db *gorm.DB) *Options {
	o.Db = db
	return o
}

func (o *Options) SetDbId(v string) *Options {
	o.DbId = v
	return o
}

func (o *Options) GetDbId() string {
	return o.DbId
}

func (o *Options) SetSort(v *string) *Options {
	o.Sort = v
	return o
}

func (o *Options) GetSort() interface{} {
	return o.Sort
}

func (o *Options) Merge(opts ...*Options) *Options {
	for _, item := range opts {
		if item == nil {
			continue
		}
		if item.Db != nil {
			o.Db = item.Db
		}
		if len(item.DbId) != 0 {
			o.DbId = item.DbId
		}
		if item.MaxTime != nil {
			o.MaxTime = item.MaxTime
		}
		if item.Sort != nil {
			o.Sort = item.Sort
		}
	}
	if o.Db == nil {
		o.Db = GetDB()
	}
	return o
}

func (s session) UseTransaction(ctx context.Context, sessionFunc ddd_repository.SessionFunc) error {
	config := &gorm.Session{}
	tx := GetDB().Session(config)
	txContext := context.WithValue(ctx, _contextKey, tx)
	return sessionFunc(txContext)
}

func GetTransaction(ctx context.Context) *gorm.DB {
	db, ok := ctx.Value(_contextKey).(*gorm.DB)
	if !ok {
		config := &gorm.Session{}
		return GetDB().Session(config)
	}
	return db
}

func ParseRsqlProcess(input string, process RsqlProcess) error {
	if len(input) == 0 {
		return nil
	}
	expr, err := rsql.Parse(input)
	if err != nil {
		return errors.New(fmt.Sprintf("rsql %s expression error, %s", input, err.Error()))
	}
	err = parseProcess(expr, process)
	if err != nil {
		return errors.New(fmt.Sprintf("rsql %s parseProcess error, %s", input, err.Error()))
	}
	return nil
}

func parseProcess(expr rsql.Expression, process rsql.Process) error {
	switch expr.(type) {
	case rsql.AndExpression:
		ex, _ := expr.(rsql.AndExpression)
		process.OnAndStart()
		for i, e := range ex.Items {
			_ = parseProcess(e, process)
			if i < len(ex.Items)-1 {
				process.OnAndItem()
			}
		}
		process.OnAndEnd()
		break
	case rsql.OrExpression:
		ex, _ := expr.(rsql.OrExpression)
		process.OnOrStart()
		for i, e := range ex.Items {
			_ = parseProcess(e, process)
			if i < len(ex.Items)-1 {
				process.OnOrItem()
			}
		}
		process.OnOrEnd()
		break
	case rsql.NotEqualsComparison:
		ex, _ := expr.(rsql.NotEqualsComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnNotEquals(name, value, ex.Comparison.Val)
		break
	case rsql.EqualsComparison:
		ex, _ := expr.(rsql.EqualsComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnEquals(name, value, ex.Comparison.Val)
		break
	case rsql.LikeComparison:
		ex, _ := expr.(rsql.LikeComparison)
		name := stringutils.AsFieldName(ex.Comparison.Identifier.Val)
		value := getValue(ex.Comparison.Val)
		process.OnLike(name, value, ex.Comparison.Val)
		break
	case rsql.NotLikeComparison:
		ex, _ := expr.(rsql.NotLikeComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnNotLike(name, value, ex.Comparison.Val)
		break
	case rsql.GreaterThanComparison:
		ex, _ := expr.(rsql.GreaterThanComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnGreaterThan(name, value, ex.Comparison.Val)
		break
	case rsql.GreaterThanOrEqualsComparison:
		ex, _ := expr.(rsql.GreaterThanOrEqualsComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnGreaterThanOrEquals(name, value, ex.Comparison.Val)
		break
	case rsql.LessThanComparison:
		ex, _ := expr.(rsql.LessThanComparison)
		name := ex.Comparison.Identifier.Val
		value := getValue(ex.Comparison.Val)
		process.OnLessThan(name, value, ex.Comparison.Val)
		break
	case rsql.LessThanOrEqualsComparison:
		ex, _ := expr.(rsql.LessThanOrEqualsComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnLessThanOrEquals(name, value, ex.Comparison.Val)
		break
	case rsql.InComparison:
		ex, _ := expr.(rsql.InComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnIn(name, value, ex.Comparison.Val)
		break
	case rsql.NotInComparison:
		ex, _ := expr.(rsql.NotInComparison)
		name := ex.Comparison.Identifier.Val
		name = stringutils.AsFieldName(name)
		value := getValue(ex.Comparison.Val)
		process.OnNotIn(name, value, ex.Comparison.Val)
		break
	}
	return nil
}

func getValue(val rsql.Value) interface{} {
	var value interface{}
	switch val.(type) {
	case rsql.IntegerValue:
		value = val.(rsql.IntegerValue).Value
		break
	case rsql.BooleanValue:
		value = val.(rsql.BooleanValue).Value
		break
	case rsql.StringValue:
		value = fmt.Sprintf(`"%v"`, val.(rsql.StringValue).Value)
		break
	case rsql.DateTimeValue:
		value = fmt.Sprintf(`"%v"`, val.(rsql.DateTimeValue).Value)
		break
	case rsql.DoubleValue:
		value = val.(rsql.DoubleValue).Value
		break
	}
	return value
}

func IsErrRecordNotFound(err error) bool {
	if err == gorm.ErrRecordNotFound {
		return true
	}
	return false
}

func newFindOptions(opt *Options) *options.FindOptions {
	findOptions := &options.FindOptions{}
	findOptions.MaxTime = opt.MaxTime
	findOptions.Sort = opt.Sort
	return findOptions
}

func getSqlWhere(tenantId, filter string) (string, error) {
	p := NewSqlProcess()
	if err := ParseRsqlProcess(filter, p); err != nil {
		return "", err
	}
	return p.GetSqlWhere(tenantId).(string), nil
}

func GetDB() *gorm.DB {
	if _db != nil {
		return _db
	}
	return _db
}

func SetDB(db *gorm.DB) {
	_db = db
}
