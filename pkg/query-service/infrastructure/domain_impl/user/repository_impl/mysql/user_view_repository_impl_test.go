package mysql
import (
	"context"
    "reflect"
    "testing"
    "time"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/user/view"
	"github.com/liuxd6825/dapr-go-ddd-sdk/errors"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
)

func TestUserViewRepositoryImpl(t *testing.T) {
	id := randomutils.UUID()
	tenantId := test.TenantId
	ctx := context.Background()

	userRepos := NewUserViewRepository()
	newUser := view.NewUserView()
	upUser := view.NewUserView()

	t.Run("Create()", func(t *testing.T) {
        newUser.CaseId = "CaseId"
        newUser.CreatedTime = randomutils.PTime()
        newUser.CreatorId = "CreatorId"
        newUser.CreatorName = randomutils.NameCN()
        newUser.DeletedTime = randomutils.PTime()
        newUser.DeleterId = "DeleterId"
        newUser.DeleterName = randomutils.NameCN()
        newUser.Email = randomutils.String(10)
        newUser.Id = id
        newUser.IsDeleted = randomutils.Boolean()
        newUser.Name = randomutils.NameCN()
        newUser.Remarks = randomutils.String(10)
        newUser.TenantId = tenantId
        newUser.UpdatedTime = randomutils.PTime()
        newUser.UpdaterId = "UpdaterId"
        newUser.UpdaterName = randomutils.NameCN()

		if err := userRepos.Create(ctx, newUser); err != nil {
			t.Error(err)
			return
		}
		if foundUser, ok, err := userRepos.FindById(ctx, newUser.TenantId, newUser.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundUser)
			// 深度对比新建实体与查询实体的属性
            if ok, fieldName := equalUser(newUser, foundUser); !ok {
                t.Error(errors.ErrorOf("foundUser.%v != newUser.%v", fieldName, fieldName))
                return
            }
		}
	})

	t.Run("Update()", func(t *testing.T) {
        upUser.Id = newUser.Id
        upUser.CaseId = "CaseId"
        upUser.CreatedTime = randomutils.PTime()
        upUser.CreatorId = "CreatorId"
        upUser.CreatorName = randomutils.NameCN()
        upUser.DeletedTime = randomutils.PTime()
        upUser.DeleterId = "DeleterId"
        upUser.DeleterName = randomutils.NameCN()
        upUser.Email = randomutils.String(10)
        upUser.IsDeleted = randomutils.Boolean()
        upUser.Name = randomutils.NameCN()
        upUser.Remarks = randomutils.String(10)
        upUser.TenantId = tenantId
        upUser.UpdatedTime = randomutils.PTime()
        upUser.UpdaterId = "UpdaterId"
        upUser.UpdaterName = randomutils.NameCN()

		if err := userRepos.Update(ctx, upUser); err != nil {
			t.Error(err)
			return
		}

		if foundUser, ok, err := userRepos.FindById(ctx, upUser.TenantId, upUser.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundUser)
            // 深度对比新建实体与查询实体的属性
            if ok, fieldName := equalUser(upUser, foundUser); !ok {
                t.Error(errors.ErrorOf("upUser.%v != foundUser.%v", fieldName, fieldName))
                return
            }
		}
	})

	t.Run("FindAll()", func(t *testing.T) {
		if all, ok, err := userRepos.FindAll(ctx, newUser.TenantId); err != nil {
			t.Error(err)
			return
		} else if ok {
			t.Logf("count = %v", len(all))
		}
	})

	t.Run("FindByIds()", func(t *testing.T) {
		ids := []string{newUser.Id, "a832a377-1177-4168-89b9-b8812ea5417b"}
		if idsList, ok, err := userRepos.FindByIds(ctx, newUser.TenantId, ids); err != nil {
			t.Error(err)
			return
		} else if ok {
			t.Logf(" count = %v", len(idsList))
		}
	})

    t.Run("FindPagingQuery()", func(t *testing.T) {
        query := ddd_repository.NewFindPagingQuery()
        query.SetTenantId(tenantId)
        query.SetSort(" Remarks, CreatedTime, UpdaterName, IsDeleted, CaseId, CreatorName, Name, DeleterId, DeleterName, DeletedTime, UpdatedTime, Id, Email, TenantId, CreatorId, UpdaterId")
        query.SetFilter(fmt.Sprintf("id=='%v'", upUser.Id))

        if res, isFound, err := userRepos.FindPaging(ctx, query); err != nil {
            t.Error(err)
        } else if isFound {
            t.Log(res.Data)
        } else {
            t.Log("NotFound")
        }
    })

	t.Run("DeleteById()", func(t *testing.T) {
		if err := userRepos.DeleteById(ctx, upUser.TenantId, upUser.Id); err != nil {
			t.Error(err)
			return
		}
	})

}

// equalUser 实体对比
func equalUser(v1 *view.UserView, v2 *view.UserView)  (bool, string) {
    if !reflect.DeepEqual(v1.CaseId, v2.CaseId) {
        return false, "CaseId"
    }
    if !reflect.DeepEqual(v1.CreatedTime, v2.CreatedTime) {
        return false, "CreatedTime"
    }
    if !reflect.DeepEqual(v1.CreatorId, v2.CreatorId) {
        return false, "CreatorId"
    }
    if !reflect.DeepEqual(v1.CreatorName, v2.CreatorName) {
        return false, "CreatorName"
    }
    if !reflect.DeepEqual(v1.DeletedTime, v2.DeletedTime) {
        return false, "DeletedTime"
    }
    if !reflect.DeepEqual(v1.DeleterId, v2.DeleterId) {
        return false, "DeleterId"
    }
    if !reflect.DeepEqual(v1.DeleterName, v2.DeleterName) {
        return false, "DeleterName"
    }
    if !reflect.DeepEqual(v1.Email, v2.Email) {
        return false, "Email"
    }
    if !reflect.DeepEqual(v1.Id, v2.Id) {
        return false, "Id"
    }
    if !reflect.DeepEqual(v1.IsDeleted, v2.IsDeleted) {
        return false, "IsDeleted"
    }
    if !reflect.DeepEqual(v1.Name, v2.Name) {
        return false, "Name"
    }
    if !reflect.DeepEqual(v1.Remarks, v2.Remarks) {
        return false, "Remarks"
    }
    if !reflect.DeepEqual(v1.TenantId, v2.TenantId) {
        return false, "TenantId"
    }
    if !reflect.DeepEqual(v1.UpdatedTime, v2.UpdatedTime) {
        return false, "UpdatedTime"
    }
    if !reflect.DeepEqual(v1.UpdaterId, v2.UpdaterId) {
        return false, "UpdaterId"
    }
    if !reflect.DeepEqual(v1.UpdaterName, v2.UpdaterName) {
        return false, "UpdaterName"
    }
    return true , ""
}