package mongodb
import (
	"context"
    "reflect"
    "testing"
    "time"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/sale_bill/view"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"github.com/liuxd6825/dapr-go-ddd-sdk/errors"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
)

func TestSaleItemViewRepositoryImpl(t *testing.T) {
	id := randomutils.UUID()
	tenantId := test.TenantId
	ctx := context.Background()

	saleItemRepos := NewSaleItemViewRepository()
	newSaleItem := view.NewSaleItemView()
	upSaleItem := view.NewSaleItemView()

	t.Run("Create()", func(t *testing.T) {
        newSaleItem.CaseId = "CaseId"
        newSaleItem.CreatedTime = randomutils.PTime()
        newSaleItem.CreatorId = "CreatorId"
        newSaleItem.CreatorName = randomutils.NameCN()
        newSaleItem.DeletedTime = randomutils.PTime()
        newSaleItem.DeleterId = "DeleterId"
        newSaleItem.DeleterName = randomutils.NameCN()
        newSaleItem.Id = id
        newSaleItem.InventoryId = "InventoryId"
        newSaleItem.InventoryName = randomutils.NameCN()
        newSaleItem.IsDeleted = randomutils.Boolean()
        newSaleItem.Money = randomutils.Float64()
        newSaleItem.Quantity = randomutils.Int64()
        newSaleItem.Remarks = randomutils.String(10)
        newSaleItem.SaleBillId = "SaleBillId"
        newSaleItem.TenantId = tenantId
        newSaleItem.UpdatedTime = randomutils.PTime()
        newSaleItem.UpdaterId = "UpdaterId"
        newSaleItem.UpdaterName = randomutils.NameCN()

		if err := saleItemRepos.Create(ctx, newSaleItem); err != nil {
			t.Error(err)
			return
		}
		if foundSaleItem, ok, err := saleItemRepos.FindById(ctx, newSaleItem.TenantId, newSaleItem.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundSaleItem)
			// 深度对比新建实体与查询实体的属性
            if ok, fieldName := equalSaleItem(newSaleItem, foundSaleItem); !ok {
                t.Error(errors.ErrorOf("foundSaleItem.%v != newSaleItem.%v", fieldName, fieldName))
                return
            }
		}
	})

	t.Run("Update()", func(t *testing.T) {
        upSaleItem.Id = newSaleItem.Id
        upSaleItem.CaseId = "CaseId"
        upSaleItem.CreatedTime = randomutils.PTime()
        upSaleItem.CreatorId = "CreatorId"
        upSaleItem.CreatorName = randomutils.NameCN()
        upSaleItem.DeletedTime = randomutils.PTime()
        upSaleItem.DeleterId = "DeleterId"
        upSaleItem.DeleterName = randomutils.NameCN()
        upSaleItem.InventoryId = "InventoryId"
        upSaleItem.InventoryName = randomutils.NameCN()
        upSaleItem.IsDeleted = randomutils.Boolean()
        upSaleItem.Money = randomutils.Float64()
        upSaleItem.Quantity = randomutils.Int64()
        upSaleItem.Remarks = randomutils.String(10)
        upSaleItem.SaleBillId = "SaleBillId"
        upSaleItem.TenantId = tenantId
        upSaleItem.UpdatedTime = randomutils.PTime()
        upSaleItem.UpdaterId = "UpdaterId"
        upSaleItem.UpdaterName = randomutils.NameCN()

		if err := saleItemRepos.Update(ctx, upSaleItem); err != nil {
			t.Error(err)
			return
		}

		if foundSaleItem, ok, err := saleItemRepos.FindById(ctx, upSaleItem.TenantId, upSaleItem.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundSaleItem)
            // 深度对比新建实体与查询实体的属性
            if ok, fieldName := equalSaleItem(upSaleItem, foundSaleItem); !ok {
                t.Error(errors.ErrorOf("upSaleItem.%v != foundSaleItem.%v", fieldName, fieldName))
                return
            }
		}
	})

	t.Run("FindAll()", func(t *testing.T) {
		if all, ok, err := saleItemRepos.FindAll(ctx, newSaleItem.TenantId); err != nil {
			t.Error(err)
			return
		} else if ok {
			t.Logf("count = %v", len(all))
		}
	})

	t.Run("FindByIds()", func(t *testing.T) {
		ids := []string{newSaleItem.Id, "a832a377-1177-4168-89b9-b8812ea5417b"}
		if idsList, ok, err := saleItemRepos.FindByIds(ctx, newSaleItem.TenantId, ids); err != nil {
			t.Error(err)
		} else if ok {
			t.Logf(" count = %v", len(idsList))
		}
	})

    t.Run("FindPagingQuery()", func(t *testing.T) {
        query := ddd_repository.NewFindPagingQuery()
        query.SetTenantId(tenantId)
        query.SetSort(" Money, SaleBillId, InventoryName, InventoryId, UpdatedTime, TenantId, IsDeleted, CreatorId, UpdaterId, DeletedTime, CreatedTime, DeleterId, DeleterName, Remarks, CaseId, Quantity, Id, CreatorName, UpdaterName")
        query.SetFilter(fmt.Sprintf("id=='%v'", upSaleItem.Id))

        if res, isFound, err := saleItemRepos.FindPaging(ctx, query); err != nil {
            t.Error(err)
        } else if isFound {
            t.Log(res.Data)
        } else {
            t.Log("NotFound")
        }
    })

	t.Run("DeleteById()", func(t *testing.T) {
		if err := saleItemRepos.DeleteById(ctx, upSaleItem.TenantId, upSaleItem.Id); err != nil {
			t.Error(err)
			return
		}
	})

}

// equalSaleItem 实体对比
func equalSaleItem(v1 *view.SaleItemView, v2 *view.SaleItemView)  (bool, string) {
    if !reflect.DeepEqual(v1.CaseId, v2.CaseId) {
        return false, "CaseId"
    }
    if !reflect.DeepEqual(v1.CreatedTime, v2.CreatedTime) {
        return false, "CreatedTime"
    }
    if !reflect.DeepEqual(v1.CreatorId, v2.CreatorId) {
        return false, "CreatorId"
    }
    if !reflect.DeepEqual(v1.CreatorName, v2.CreatorName) {
        return false, "CreatorName"
    }
    if !reflect.DeepEqual(v1.DeletedTime, v2.DeletedTime) {
        return false, "DeletedTime"
    }
    if !reflect.DeepEqual(v1.DeleterId, v2.DeleterId) {
        return false, "DeleterId"
    }
    if !reflect.DeepEqual(v1.DeleterName, v2.DeleterName) {
        return false, "DeleterName"
    }
    if !reflect.DeepEqual(v1.Id, v2.Id) {
        return false, "Id"
    }
    if !reflect.DeepEqual(v1.InventoryId, v2.InventoryId) {
        return false, "InventoryId"
    }
    if !reflect.DeepEqual(v1.InventoryName, v2.InventoryName) {
        return false, "InventoryName"
    }
    if !reflect.DeepEqual(v1.IsDeleted, v2.IsDeleted) {
        return false, "IsDeleted"
    }
    if !reflect.DeepEqual(v1.Money, v2.Money) {
        return false, "Money"
    }
    if !reflect.DeepEqual(v1.Quantity, v2.Quantity) {
        return false, "Quantity"
    }
    if !reflect.DeepEqual(v1.Remarks, v2.Remarks) {
        return false, "Remarks"
    }
    if !reflect.DeepEqual(v1.SaleBillId, v2.SaleBillId) {
        return false, "SaleBillId"
    }
    if !reflect.DeepEqual(v1.TenantId, v2.TenantId) {
        return false, "TenantId"
    }
    if !reflect.DeepEqual(v1.UpdatedTime, v2.UpdatedTime) {
        return false, "UpdatedTime"
    }
    if !reflect.DeepEqual(v1.UpdaterId, v2.UpdaterId) {
        return false, "UpdaterId"
    }
    if !reflect.DeepEqual(v1.UpdaterName, v2.UpdaterName) {
        return false, "UpdaterName"
    }
    return true , ""
}