package neo4j
import (
	"context"
    "reflect"
    "testing"
    "time"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/sale_bill/view"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
    "github.com/liuxd6825/dapr-go-ddd-sdk/ddd/ddd_repository"
	"github.com/liuxd6825/dapr-go-ddd-sdk/errors"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
)

func TestSaleBillViewRepositoryImpl(t *testing.T) {
	id := randomutils.UUID()
	tenantId := test.TenantId
	ctx := context.Background()

	saleBillRepos := NewSaleBillViewRepository()
	newSaleBill := view.NewSaleBillView()
	upSaleBill := view.NewSaleBillView()

	t.Run("Create()", func(t *testing.T) {
        newSaleBill.CaseId = "CaseId"
        newSaleBill.CreatedTime = randomutils.PTime()
        newSaleBill.CreatorId = "CreatorId"
        newSaleBill.CreatorName = randomutils.NameCN()
        newSaleBill.DeletedTime = randomutils.PTime()
        newSaleBill.DeleterId = "DeleterId"
        newSaleBill.DeleterName = randomutils.NameCN()
        newSaleBill.Id = id
        newSaleBill.IsDeleted = randomutils.Boolean()
        newSaleBill.Remarks = randomutils.String(10)
        newSaleBill.SaleMoney = randomutils.Float64()
        newSaleBill.SaleTime = randomutils.PTime()
        newSaleBill.Statue = randomutils.String(10)
        newSaleBill.TenantId = tenantId
        newSaleBill.UpdatedTime = randomutils.PTime()
        newSaleBill.UpdaterId = "UpdaterId"
        newSaleBill.UpdaterName = randomutils.NameCN()
        newSaleBill.UserId = "UserId"
        newSaleBill.UserName = randomutils.NameCN()

		if err := saleBillRepos.Create(ctx, newSaleBill); err != nil {
			t.Error(err)
			return
		}
		if foundSaleBill, ok, err := saleBillRepos.FindById(ctx, newSaleBill.TenantId, newSaleBill.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundSaleBill)
			// 深度对比新建实体与查询实体的属性
            if err := newSaleBill.Equal(foundSaleBill); err!=nil {
                t.Error(err)
            }
		}
	})

	t.Run("Update()", func(t *testing.T) {
        upSaleBill.Id = newSaleBill.Id
        upSaleBill.CaseId = "CaseId"
        upSaleBill.CreatedTime = randomutils.PTime()
        upSaleBill.CreatorId = "CreatorId"
        upSaleBill.CreatorName = randomutils.NameCN()
        upSaleBill.DeletedTime = randomutils.PTime()
        upSaleBill.DeleterId = "DeleterId"
        upSaleBill.DeleterName = randomutils.NameCN()
        upSaleBill.IsDeleted = randomutils.Boolean()
        upSaleBill.Remarks = randomutils.String(10)
        upSaleBill.SaleMoney = randomutils.Float64()
        upSaleBill.SaleTime = randomutils.PTime()
        upSaleBill.Statue = randomutils.String(10)
        upSaleBill.TenantId = tenantId
        upSaleBill.UpdatedTime = randomutils.PTime()
        upSaleBill.UpdaterId = "UpdaterId"
        upSaleBill.UpdaterName = randomutils.NameCN()
        upSaleBill.UserId = "UserId"
        upSaleBill.UserName = randomutils.NameCN()

		if err := saleBillRepos.Update(ctx, upSaleBill); err != nil {
			t.Error(err)
			return
		}

		if foundSaleBill, ok, err := saleBillRepos.FindById(ctx, upSaleBill.TenantId, upSaleBill.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundSaleBill)
            // 深度对比新建实体与查询实体的属性
            if err := upSaleBill.Equal(foundSaleBill); err!=nil {
                t.Error(err)
            }
		}
	})

	t.Run("FindAll()", func(t *testing.T) {
		if all, ok, err := saleBillRepos.FindAll(ctx, newSaleBill.TenantId); err != nil {
			t.Error(err)
			return
		} else if ok {
			t.Logf("count = %v", len(all))
		}
	})

	t.Run("FindByIds()", func(t *testing.T) {
		ids := []string{newSaleBill.Id, "a832a377-1177-4168-89b9-b8812ea5417b"}
		if idsList, ok, err := saleBillRepos.FindByIds(ctx, newSaleBill.TenantId, ids); err != nil {
			t.Error(err)
			return
		} else if ok {
			t.Logf(" count = %v", len(idsList))
		}
	})

    t.Run("FindPagingQuery()", func(t *testing.T) {
        query := ddd_repository.NewFindPagingQuery()
        query.SetTenantId(tenantId)
        query.SetSort(" UpdaterId, CreatorId, DeleterId, CreatorName, UserId, SaleMoney, SaleItems, IsDeleted, TenantId, SaleTime, Statue, DeleterName, Remarks, UserName, CreatedTime, UpdatedTime, CaseId, Id, UpdaterName, DeletedTime")
        query.SetFilter(fmt.Sprintf("id=='%v'", upSaleBill.Id))

        if res, isFound, err := saleBillRepos.FindPaging(ctx, query); err != nil {
            t.Error(err)
        } else  if isFound {
            t.Log(res.Data)
        } else {
            t.Log("NotFound")
        }
    })

	t.Run("DeleteById()", func(t *testing.T) {
		if err := saleBillRepos.DeleteById(ctx, upSaleBill.TenantId, upSaleBill.Id); err != nil {
			t.Error(err)
			return
		}
	})

}

// equalSaleBill 实体对比
func equalSaleBill(v1 *view.SaleBillView, v2 *view.SaleBillView)  (bool, string) {
    if !reflect.DeepEqual(v1.CaseId, v2.CaseId) {
        return false, "CaseId"
    }
    if !reflect.DeepEqual(v1.CreatedTime, v2.CreatedTime) {
        return false, "CreatedTime"
    }
    if !reflect.DeepEqual(v1.CreatorId, v2.CreatorId) {
        return false, "CreatorId"
    }
    if !reflect.DeepEqual(v1.CreatorName, v2.CreatorName) {
        return false, "CreatorName"
    }
    if !reflect.DeepEqual(v1.DeletedTime, v2.DeletedTime) {
        return false, "DeletedTime"
    }
    if !reflect.DeepEqual(v1.DeleterId, v2.DeleterId) {
        return false, "DeleterId"
    }
    if !reflect.DeepEqual(v1.DeleterName, v2.DeleterName) {
        return false, "DeleterName"
    }
    if !reflect.DeepEqual(v1.Id, v2.Id) {
        return false, "Id"
    }
    if !reflect.DeepEqual(v1.IsDeleted, v2.IsDeleted) {
        return false, "IsDeleted"
    }
    if !reflect.DeepEqual(v1.Remarks, v2.Remarks) {
        return false, "Remarks"
    }
    if !reflect.DeepEqual(v1.SaleItems, v2.SaleItems) {
        return false, "SaleItems"
    }
    if !reflect.DeepEqual(v1.SaleMoney, v2.SaleMoney) {
        return false, "SaleMoney"
    }
    if !reflect.DeepEqual(v1.SaleTime, v2.SaleTime) {
        return false, "SaleTime"
    }
    if !reflect.DeepEqual(v1.Statue, v2.Statue) {
        return false, "Statue"
    }
    if !reflect.DeepEqual(v1.TenantId, v2.TenantId) {
        return false, "TenantId"
    }
    if !reflect.DeepEqual(v1.UpdatedTime, v2.UpdatedTime) {
        return false, "UpdatedTime"
    }
    if !reflect.DeepEqual(v1.UpdaterId, v2.UpdaterId) {
        return false, "UpdaterId"
    }
    if !reflect.DeepEqual(v1.UpdaterName, v2.UpdaterName) {
        return false, "UpdaterName"
    }
    if !reflect.DeepEqual(v1.UserId, v2.UserId) {
        return false, "UserId"
    }
    if !reflect.DeepEqual(v1.UserName, v2.UserName) {
        return false, "UserName"
    }
    return true , ""
}