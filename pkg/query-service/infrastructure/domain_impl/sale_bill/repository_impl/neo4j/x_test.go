package neo4j

import (
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
)

func init() {
    test.InitQuery()
}
