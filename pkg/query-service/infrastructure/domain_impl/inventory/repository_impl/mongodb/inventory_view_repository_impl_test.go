package mongodb
import (
	"context"
    "reflect"
    "testing"
    "time"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/domain/inventory/view"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"github.com/liuxd6825/dapr-go-ddd-sdk/errors"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
)

func TestInventoryViewRepositoryImpl(t *testing.T) {
	id := randomutils.UUID()
	tenantId := test.TenantId
	ctx := context.Background()

	inventoryRepos := NewInventoryViewRepository()
	newInventory := view.NewInventoryView()
	upInventory := view.NewInventoryView()

	t.Run("Create()", func(t *testing.T) {
        newInventory.Brand = randomutils.String(10)
        newInventory.CaseId = "CaseId"
        newInventory.CreatedTime = randomutils.PTime()
        newInventory.CreatorId = "CreatorId"
        newInventory.CreatorName = randomutils.NameCN()
        newInventory.DeletedTime = randomutils.PTime()
        newInventory.DeleterId = "DeleterId"
        newInventory.DeleterName = randomutils.NameCN()
        newInventory.Id = id
        newInventory.IsDeleted = randomutils.Boolean()
        newInventory.Keywords = randomutils.String(10)
        newInventory.Name = randomutils.NameCN()
        newInventory.Remarks = randomutils.String(10)
        newInventory.Spec = randomutils.String(10)
        newInventory.TenantId = tenantId
        newInventory.UpdatedTime = randomutils.PTime()
        newInventory.UpdaterId = "UpdaterId"
        newInventory.UpdaterName = randomutils.NameCN()

		if err := inventoryRepos.Create(ctx, newInventory); err != nil {
			t.Error(err)
			return
		}
		if foundInventory, ok, err := inventoryRepos.FindById(ctx, newInventory.TenantId, newInventory.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundInventory)
			// 深度对比新建实体与查询实体的属性
            if ok, fieldName := equalInventory(newInventory, foundInventory); !ok {
                t.Error(errors.ErrorOf("foundInventory.%v != newInventory.%v", fieldName, fieldName))
                return
            }
		}
	})

	t.Run("Update()", func(t *testing.T) {
        upInventory.Id = newInventory.Id
        upInventory.Brand = randomutils.String(10)
        upInventory.CaseId = "CaseId"
        upInventory.CreatedTime = randomutils.PTime()
        upInventory.CreatorId = "CreatorId"
        upInventory.CreatorName = randomutils.NameCN()
        upInventory.DeletedTime = randomutils.PTime()
        upInventory.DeleterId = "DeleterId"
        upInventory.DeleterName = randomutils.NameCN()
        upInventory.IsDeleted = randomutils.Boolean()
        upInventory.Keywords = randomutils.String(10)
        upInventory.Name = randomutils.NameCN()
        upInventory.Remarks = randomutils.String(10)
        upInventory.Spec = randomutils.String(10)
        upInventory.TenantId = tenantId
        upInventory.UpdatedTime = randomutils.PTime()
        upInventory.UpdaterId = "UpdaterId"
        upInventory.UpdaterName = randomutils.NameCN()

		if err := inventoryRepos.Update(ctx, upInventory); err != nil {
			t.Error(err)
			return
		}

		if foundInventory, ok, err := inventoryRepos.FindById(ctx, upInventory.TenantId, upInventory.Id); err != nil {
			t.Error(err)
			return
		} else if !ok {
			t.Error(errors.New("error not found"))
			return
		} else {
			t.Log("found : ", foundInventory)
            // 深度对比新建实体与查询实体的属性
            if ok, fieldName := equalInventory(upInventory, foundInventory); !ok {
                t.Error(errors.ErrorOf("upInventory.%v != foundInventory.%v", fieldName, fieldName))
                return
            }
		}
	})

	t.Run("FindAll()", func(t *testing.T) {
		if all, ok, err := inventoryRepos.FindAll(ctx, newInventory.TenantId); err != nil {
			t.Error(err)
			return
		} else if ok {
			t.Logf("count = %v", len(all))
		}
	})

	t.Run("FindByIds()", func(t *testing.T) {
		ids := []string{newInventory.Id, "a832a377-1177-4168-89b9-b8812ea5417b"}
		if idsList, ok, err := inventoryRepos.FindByIds(ctx, newInventory.TenantId, ids); err != nil {
			t.Error(err)
		} else if ok {
			t.Logf(" count = %v", len(idsList))
		}
	})

    t.Run("FindPagingQuery()", func(t *testing.T) {
        query := ddd_repository.NewFindPagingQuery()
        query.SetTenantId(tenantId)
        query.SetSort(" Id, Spec, Remarks, DeleterName, UpdaterName, Keywords, TenantId, CreatedTime, UpdaterId, CaseId, DeleterId, CreatorName, DeletedTime, CreatorId, Name, IsDeleted, Brand, UpdatedTime")
        query.SetFilter(fmt.Sprintf("id=='%v'", upInventory.Id))

        if res, isFound, err := inventoryRepos.FindPaging(ctx, query); err != nil {
            t.Error(err)
        } else if isFound {
            t.Log(res.Data)
        } else {
            t.Log("NotFound")
        }
    })

	t.Run("DeleteById()", func(t *testing.T) {
		if err := inventoryRepos.DeleteById(ctx, upInventory.TenantId, upInventory.Id); err != nil {
			t.Error(err)
			return
		}
	})

}

// equalInventory 实体对比
func equalInventory(v1 *view.InventoryView, v2 *view.InventoryView)  (bool, string) {
    if !reflect.DeepEqual(v1.Brand, v2.Brand) {
        return false, "Brand"
    }
    if !reflect.DeepEqual(v1.CaseId, v2.CaseId) {
        return false, "CaseId"
    }
    if !reflect.DeepEqual(v1.CreatedTime, v2.CreatedTime) {
        return false, "CreatedTime"
    }
    if !reflect.DeepEqual(v1.CreatorId, v2.CreatorId) {
        return false, "CreatorId"
    }
    if !reflect.DeepEqual(v1.CreatorName, v2.CreatorName) {
        return false, "CreatorName"
    }
    if !reflect.DeepEqual(v1.DeletedTime, v2.DeletedTime) {
        return false, "DeletedTime"
    }
    if !reflect.DeepEqual(v1.DeleterId, v2.DeleterId) {
        return false, "DeleterId"
    }
    if !reflect.DeepEqual(v1.DeleterName, v2.DeleterName) {
        return false, "DeleterName"
    }
    if !reflect.DeepEqual(v1.Id, v2.Id) {
        return false, "Id"
    }
    if !reflect.DeepEqual(v1.IsDeleted, v2.IsDeleted) {
        return false, "IsDeleted"
    }
    if !reflect.DeepEqual(v1.Keywords, v2.Keywords) {
        return false, "Keywords"
    }
    if !reflect.DeepEqual(v1.Name, v2.Name) {
        return false, "Name"
    }
    if !reflect.DeepEqual(v1.Remarks, v2.Remarks) {
        return false, "Remarks"
    }
    if !reflect.DeepEqual(v1.Spec, v2.Spec) {
        return false, "Spec"
    }
    if !reflect.DeepEqual(v1.TenantId, v2.TenantId) {
        return false, "TenantId"
    }
    if !reflect.DeepEqual(v1.UpdatedTime, v2.UpdatedTime) {
        return false, "UpdatedTime"
    }
    if !reflect.DeepEqual(v1.UpdaterId, v2.UpdaterId) {
        return false, "UpdaterId"
    }
    if !reflect.DeepEqual(v1.UpdaterName, v2.UpdaterName) {
        return false, "UpdaterName"
    }
    return true , ""
}