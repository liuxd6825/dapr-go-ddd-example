package facade

import (
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/userinterface/rest/user/dto"
	"testing"
)

func TestUserQueryApi(t *testing.T) {
	e := httptest.New(t, app)

	ids := []string{""}
	tenantId := test.TenantId

	t.Run("FindAll", func(t *testing.T) {
		var list []*dto.UserDto
		resp := e.GET("/api/v1.0/tenants/{tenantId}/users:all", tenantId).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &list); err != nil {
			t.Error(err)
		}
		for _, item := range list {
			ids = append(ids, item.Id)
		}
	})

	t.Run("FindPaging", func(t *testing.T) {
		var data dto.UserFindPagingResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/users", tenantId).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindById", func(t *testing.T) {
		var data *dto.UserFindByIdResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/users/{id}", tenantId, ids[len(ids)-1]).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindByIds", func(t *testing.T) {
		var data *dto.UserFindByIdsResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/users:ids", tenantId, ids).Expect()
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

}



