package facade

import (
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/userinterface/rest/sale_bill/dto"
	"testing"
)

func TestSaleItemQueryApi(t *testing.T) {
	e := httptest.New(t, app)

	ids := []string{""}
	tenantId := test.TenantId

	t.Run("FindAll", func(t *testing.T) {
		var list []*dto.SaleItemDto
		resp := e.GET("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items:all", tenantId).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &list); err != nil {
			t.Error(err)
		}
		for _, item := range list {
			ids = append(ids, item.Id)
		}
	})

	t.Run("FindPaging", func(t *testing.T) {
		var data dto.SaleItemFindPagingResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items", tenantId).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindById", func(t *testing.T) {
		var data *dto.SaleItemFindByIdResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items/{id}", tenantId, ids[len(ids)-1]).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindByIds", func(t *testing.T) {
		var data *dto.SaleItemFindByIdsResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items:ids", tenantId, ids).Expect()
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})
	t.Run("FindBySaleBillId", func(t *testing.T) {
		var data *dto.SaleItemFindByIdsResponse
		saleBillId := "SaleBillId"
		resp := e.GET("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items:saleBillId?saleBillId='%v'", tenantId, saleBillId).Expect()
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

}



