package facade

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
)

var app *iris.Application

func init() {
	test.InitQuery()

	app = iris.New()
	mvc.Configure(app.Party("/api/v1.0"), func(app *mvc.Application) {
		app.Handle(NewSaleBillQueryApi())
        app.Handle(NewSaleItemQueryApi())
	})
}
