// 添加注释以描述 server 信息
// @title           github.com/liuxu6825/dapr-go-ddd-example query-service API
// @version         1.0
// @description     案件分析图 查询服务
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:9020
// @BasePath  /api/v1

// @securityDefinitions.basic  BasicAuth
package rest
