package facade

import (
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/infrastructure/test"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/userinterface/rest/inventory/dto"
	"testing"
)

func TestInventoryQueryApi(t *testing.T) {
	e := httptest.New(t, app)

	ids := []string{""}
	tenantId := test.TenantId

	t.Run("FindAll", func(t *testing.T) {
		var list []*dto.InventoryDto
		resp := e.GET("/api/v1.0/tenants/{tenantId}/inventories:all", tenantId).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &list); err != nil {
			t.Error(err)
		}
		for _, item := range list {
			ids = append(ids, item.Id)
		}
	})

	t.Run("FindPaging", func(t *testing.T) {
		var data dto.InventoryFindPagingResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/inventories", tenantId).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindById", func(t *testing.T) {
		var data *dto.InventoryFindByIdResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/inventories/{id}", tenantId, ids[len(ids)-1]).Expect().Status(httptest.StatusOK)
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindByIds", func(t *testing.T) {
		var data *dto.InventoryFindByIdsResponse
		resp := e.GET("/api/v1.0/tenants/{tenantId}/inventories:ids", tenantId, ids).Expect()
		if err := test.GetResponseData(t, resp, &data); err != nil {
			t.Error(err)
		}
	})

}



