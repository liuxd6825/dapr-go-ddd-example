package facade


import (
	"testing"
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/userinterface/rest/inventory/dto"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestInventoryCommandApi(t *testing.T) {
	e := httptest.New(t, app)

	id := randomutils.UUID()
	tenantId := test.TenantId
	
	t.Run("InventoryCreateCommand", func(t *testing.T) {
        req := dto.InventoryCreateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.InventoryCreateCommandRequestData{
                Brand : randomutils.String(10),
                CaseId : "CaseId",
                Id: id,
                Keywords : randomutils.String(10),
                Name : randomutils.NameCN(),
                Remarks : randomutils.String(10),
                Spec : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.POST("/api/v1.0/tenants/{tenantId}/inventories", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("InventoryUpdateCommand", func(t *testing.T) {
        req := dto.InventoryUpdateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.InventoryUpdateCommandRequestData{
                Brand : randomutils.String(10),
                CaseId : "CaseId",
                Id: id,
                Keywords : randomutils.String(10),
                Name : randomutils.NameCN(),
                Remarks : randomutils.String(10),
                Spec : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.PATCH("/api/v1.0/tenants/{tenantId}/inventories", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})

	t.Run("FindAggregateById", func(t *testing.T) {
		body := e.GET("/api/v1.0/tenants/{tenantId}/inventories/{id}", tenantId, id).Expect().Status(httptest.StatusOK).Body()
		t.Log(*body)
	})

}
