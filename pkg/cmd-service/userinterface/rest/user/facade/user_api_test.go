package facade


import (
	"testing"
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/userinterface/rest/user/dto"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestUserCommandApi(t *testing.T) {
	e := httptest.New(t, app)

	id := randomutils.UUID()
	tenantId := test.TenantId
	
	t.Run("UserDeleteCommand", func(t *testing.T) {
        req := dto.UserDeleteCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.UserDeleteCommandRequestData{
                CaseId : "CaseId",
                Id: id,
                Remarks : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.DELETE("/api/v1.0/tenants/{tenantId}/users", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("UserCreateCommand", func(t *testing.T) {
        req := dto.UserCreateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.UserCreateCommandRequestData{
                CaseId : "CaseId",
                Email : randomutils.String(10),
                Id: id,
                Name : randomutils.NameCN(),
                Remarks : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.POST("/api/v1.0/tenants/{tenantId}/users", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("UserUpdateCommand", func(t *testing.T) {
        req := dto.UserUpdateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.UserUpdateCommandRequestData{
                CaseId : "CaseId",
                Email : randomutils.String(10),
                Id: id,
                Name : randomutils.NameCN(),
                Remarks : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.PATCH("/api/v1.0/tenants/{tenantId}/users", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})

	t.Run("FindAggregateById", func(t *testing.T) {
		body := e.GET("/api/v1.0/tenants/{tenantId}/users/{id}", tenantId, id).Expect().Status(httptest.StatusOK).Body()
		t.Log(*body)
	})

}
