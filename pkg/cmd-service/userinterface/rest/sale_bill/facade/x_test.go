package facade

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/event"
)

var app *iris.Application

func init() {
    test.InitCommand(event.GetRegisterEventTypes())

	app = iris.New()
	mvc.Configure(app.Party("/api/v1.0"), func(app *mvc.Application) {
		app.Handle(NewSaleBillCommandApi())
        app.Handle(NewSaleItemCommandApi())
	})

}
