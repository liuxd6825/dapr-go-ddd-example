package facade


import (
	"testing"
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/userinterface/rest/sale_bill/dto"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestSaleBillCommandApi(t *testing.T) {
	e := httptest.New(t, app)

	id := randomutils.UUID()
	tenantId := test.TenantId
	
	t.Run("SaleBillCreateCommand", func(t *testing.T) {
        req := dto.SaleBillCreateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleBillCreateCommandRequestData{
                CaseId : "CaseId",
                Id: id,
                Remarks : randomutils.String(10),
                SaleMoney : randomutils.Float64(),
                SaleTime : randomutils.PJsonTime(),
                TenantId : tenantId,
                UserId : "UserId",
                UserName : randomutils.NameCN(),
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.POST("/api/v1.0/tenants/{tenantId}/sale-bills", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("SaleBillUpdateCommand", func(t *testing.T) {
        req := dto.SaleBillUpdateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleBillUpdateCommandRequestData{
                CaseId : "CaseId",
                Id: id,
                Remarks : randomutils.String(10),
                SaleMoney : randomutils.Float64(),
                SaleTime : randomutils.PJsonTime(),
                TenantId : tenantId,
                UserId : "UserId",
                UserName : randomutils.NameCN(),
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.PATCH("/api/v1.0/tenants/{tenantId}/sale-bills", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("SaleBillConfirmCommand", func(t *testing.T) {
        req := dto.SaleBillConfirmCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleBillConfirmCommandRequestData{
                CaseId : "CaseId",
                Id: id,
                Remarks : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.PATCH("/api/v1.0/tenants/{tenantId}/sale-bills:confirm", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("SaleBillDeleteCommand", func(t *testing.T) {
        req := dto.SaleBillDeleteCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleBillDeleteCommandRequestData{
                CaseId : "CaseId",
                Id: id,
                Remarks : randomutils.String(10),
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.DELETE("/api/v1.0/tenants/{tenantId}/sale-bills", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})

	t.Run("FindAggregateById", func(t *testing.T) {
		body := e.GET("/api/v1.0/tenants/{tenantId}/sale-bills/{id}", tenantId, id).Expect().Status(httptest.StatusOK).Body()
		t.Log(*body)
	})

}
