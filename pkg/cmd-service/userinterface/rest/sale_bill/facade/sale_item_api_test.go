package facade


import (
	"testing"
	"github.com/kataras/iris/v12/httptest"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/userinterface/rest/sale_bill/dto"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestSaleItemCommandApi(t *testing.T) {
	e := httptest.New(t, app)

	id := randomutils.UUID()
	tenantId := test.TenantId
	
	t.Run("SaleItemUpdateCommand", func(t *testing.T) {
        req := dto.SaleItemUpdateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleItemUpdateCommandRequestData{
                Items:[]*dto.SaleItemUpdateItem{
                    &dto.SaleItemUpdateItem{
                        Id:id,
                        InventoryId:"InventoryId",
                        Quantity:randomutils.Int64(),
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        InventoryName:randomutils.NameCN(),
                        Money:randomutils.Float64(),
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                    },
                },
                SaleBillId : "SaleBillId",
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.PATCH("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("SaleItemDeleteCommand", func(t *testing.T) {
        req := dto.SaleItemDeleteCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleItemDeleteCommandRequestData{
                Items:[]*dto.SaleItemDeleteItem{
                    &dto.SaleItemDeleteItem{
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                        Id:id,
                    },
                },
                SaleBillId : "SaleBillId",
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.DELETE("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})
	
	t.Run("SaleItemCreateCommand", func(t *testing.T) {
        req := dto.SaleItemCreateCommandRequest{
            CommandId: randomutils.UUID(),
            IsValidOnly: false,
            Data: dto.SaleItemCreateCommandRequestData{
                Items:[]*dto.SaleItemCreateItem{
                    &dto.SaleItemCreateItem{
                        SaleBillId:"SaleBillId",
                        Id:id,
                        InventoryId:"InventoryId",
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Remarks:randomutils.String(10),
                        InventoryName:randomutils.NameCN(),
                        Quantity:randomutils.Int64(),
                        Money:randomutils.Float64(),
                    },
                },
                SaleBillId : "SaleBillId",
                TenantId : tenantId,
            },
        }
        t.Logf("CommandId = %v ", req.CommandId)
        e.POST("/api/v1.0/tenants/{tenantId}/sale-bills/sale-items", tenantId).WithJSON(req).Expect().Status(httptest.StatusOK)
	})

}
