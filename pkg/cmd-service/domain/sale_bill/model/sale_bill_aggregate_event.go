package model

import (
    "context"
    "time"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/command"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/event"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/field"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/factory"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/utils"
    "github.com/liuxd6825/dapr-go-ddd-sdk/ddd"
)
//
// OnSaleBillConfirmEventV1s0
// @Description: SaleBillConfirmEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleBillConfirmEventV1s0(ctx context.Context, e *event.SaleBillConfirmEvent) error {
	//TODO implement me
	panic("implement me")
}
//
// OnSaleBillCreateEventV1s0
// @Description: SaleBillCreateEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleBillCreateEventV1s0(ctx context.Context, e *event.SaleBillCreateEvent) error {
    return utils.Mapper(e.Data, a)
}
//
// OnSaleBillDeleteEventV1s0
// @Description: SaleBillDeleteEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleBillDeleteEventV1s0(ctx context.Context, e *event.SaleBillDeleteEvent) error {
	a.IsDeleted = true
	return nil
}
//
// OnSaleBillUpdateEventV1s0
// @Description: SaleBillUpdateEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleBillUpdateEventV1s0(ctx context.Context, e *event.SaleBillUpdateEvent) error {
    return utils.MaskMapperRemove(e.Data, a, e.UpdateMask, aggMapperRemove)
}
//
// OnSaleItemCreateEventV1s0
// @Description: SaleItemCreateEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleItemCreateEventV1s0(ctx context.Context, e *event.SaleItemCreateEvent) error {
    for _, item := range e.Data.Items {
        if _, err := a.SaleItems.AddMapper(ctx, item.Id, item); err != nil {
            return err
        }
    }
    return nil
}
//
// OnSaleItemDeleteEventV1s0
// @Description: SaleItemDeleteEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleItemDeleteEventV1s0(ctx context.Context, e *event.SaleItemDeleteEvent) error {
    for _, item := range e.Data.Items {
        if err := a.SaleItems.DeleteById(ctx, item.Id); err != nil {
            return err
        }
    }
    return nil
}
//
// OnSaleItemUpdateEventV1s0
// @Description: SaleItemUpdateEvent 领域事件 事件溯源处理器
// @receiver a
// @param ctx 上下文件
// @param event 领域事件
// @return err 错误
//
func (a *SaleBillAggregate) OnSaleItemUpdateEventV1s0(ctx context.Context, e *event.SaleItemUpdateEvent) error {
    for _, item := range e.Data.Items {
        if _, err := a.SaleItems.AddMapper(ctx, item.Id, item); err != nil {
            return err
        }
    }
    return nil
}

