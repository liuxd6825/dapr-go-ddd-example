package event

import (
    "github.com/liuxd6825/dapr-go-ddd-sdk/restapp"
)

type SaleBillEventType uint32

const (

	SaleBillConfirmEventType  SaleBillEventType = iota
	SaleBillDeleteEventType 
	SaleItemCreateEventType 
	SaleItemUpdateEventType 
	SaleItemDeleteEventType 
	SaleBillCreateEventType 
	SaleBillUpdateEventType 
)

//
// String()
// @Description: 转换成字符串
//
func (p SaleBillEventType) String() string {
	switch p {
    case SaleBillConfirmEventType:
        return "ddd-example.SaleBillConfirmEvent"
    case SaleBillDeleteEventType:
        return "ddd-example.SaleBillDeleteEvent"
    case SaleItemCreateEventType:
        return "ddd-example.SaleItemCreateEvent"
    case SaleItemUpdateEventType:
        return "ddd-example.SaleItemUpdateEvent"
    case SaleItemDeleteEventType:
        return "ddd-example.SaleItemDeleteEvent"
    case SaleBillCreateEventType:
        return "ddd-example.SaleBillCreateEvent"
    case SaleBillUpdateEventType:
        return "ddd-example.SaleBillUpdateEvent"
    default:
        return "UNKNOWN"
	}
}

//
// GetRegisterEventTypes
// @Description: 获取聚合根注册事件类型
// @return []restapp.RegisterEventType
//
func GetRegisterEventTypes() []restapp.RegisterEventType {
    return []restapp.RegisterEventType {
        {
            EventType: SaleBillConfirmEventType.String(),
            Version:   NewSaleBillConfirmEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleBillConfirmEvent{} },
        },
        {
            EventType: SaleBillCreateEventType.String(),
            Version:   NewSaleBillCreateEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleBillCreateEvent{} },
        },
        {
            EventType: SaleBillDeleteEventType.String(),
            Version:   NewSaleBillDeleteEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleBillDeleteEvent{} },
        },
        {
            EventType: SaleBillUpdateEventType.String(),
            Version:   NewSaleBillUpdateEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleBillUpdateEvent{} },
        },
        {
            EventType: SaleItemCreateEventType.String(),
            Version:   NewSaleItemCreateEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleItemCreateEvent{} },
        },
        {
            EventType: SaleItemDeleteEventType.String(),
            Version:   NewSaleItemDeleteEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleItemDeleteEvent{} },
        },
        {
            EventType: SaleItemUpdateEventType.String(),
            Version:   NewSaleItemUpdateEvent("").GetEventVersion(),
            NewFunc:   func() interface{} { return &SaleItemUpdateEvent{} },
        },
    }
}
