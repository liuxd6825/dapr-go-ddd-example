package appcmd

import (
	domain "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/user/command"
)
//
// UserDeleteAppCmd
// @Description: 应用服务层命令, 删除用户
//
type UserDeleteAppCmd struct {
	domain.UserDeleteCommand
}
//
// UserCreateAppCmd
// @Description: 应用服务层命令, 创建用户
//
type UserCreateAppCmd struct {
	domain.UserCreateCommand
}
//
// UserUpdateAppCmd
// @Description: 应用服务层命令, 更新用户
//
type UserUpdateAppCmd struct {
	domain.UserUpdateCommand
}