package service

import (
	"context"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/user/appcmd"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/user/executor"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/user/model"
	domain_service "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/user/service"
	query_dto "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/userinterface/rest/user/dto"
	base "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/base/application/service"
)

type UserAppCmdService struct {
    base.BaseAppQueryService
    userCreateCommandExecutor executor.UserCreateCommandExecutor
    userDeleteCommandExecutor executor.UserDeleteCommandExecutor
    userUpdateCommandExecutor executor.UserUpdateCommandExecutor
    findAggregateByIdExecutor executor.FindAggregateByIdExecutor
}

//
// NewUserAppCmdService
// @Description:  <no value>
// @return *UserAppCmdService
//
func NewUserAppCmdService() *UserAppCmdService {
	res := &UserAppCmdService{
        userCreateCommandExecutor: executor.GetUserCreateCommandExecutor(),
        userDeleteCommandExecutor: executor.GetUserDeleteCommandExecutor(),
        userUpdateCommandExecutor: executor.GetUserUpdateCommandExecutor(),
        findAggregateByIdExecutor: executor.GetFindAggregateByIdExecutor(),
	}
    res.Init("ddd-example-query-service", "users", "v1.0")
    return res
}

//
// UserCreate
// @Description: 创建用户
// @receiver s
// @param ctx 上下文
// @param cmd 创建用户命令DTO对象
// @return error
//
func (s *UserAppCmdService) UserCreate(ctx context.Context, appCmd *appcmd.UserCreateAppCmd) error {
	return s.userCreateCommandExecutor.Execute(ctx, appCmd)
}

//
// UserDelete
// @Description: 删除用户
// @receiver s
// @param ctx 上下文
// @param cmd 删除用户命令DTO对象
// @return error
//
func (s *UserAppCmdService) UserDelete(ctx context.Context, appCmd *appcmd.UserDeleteAppCmd) error {
	return s.userDeleteCommandExecutor.Execute(ctx, appCmd)
}

//
// UserUpdate
// @Description: 更新用户
// @receiver s
// @param ctx 上下文
// @param cmd 更新用户命令DTO对象
// @return error
//
func (s *UserAppCmdService) UserUpdate(ctx context.Context, appCmd *appcmd.UserUpdateAppCmd) error {
	return s.userUpdateCommandExecutor.Execute(ctx, appCmd)
}

//
// FindAggregateById
// @Description:
// @receiver s
// @param ctx 上下文
// @param tenantId 租户Id
// @param id 聚合根Id
// @return error
//
func (s *UserAppCmdService) FindAggregateById(ctx context.Context, tenantId string, id string) (*model.UserAggregate, bool, error) {
	return s.findAggregateByIdExecutor.Execute(ctx, tenantId, id)
}

//
// QueryById
// @Description: 按id获取<no value>投影类
// @receiver s queryAppService
// @param ctx 上下文
// @param tenantId  租户id
// @param id <no value> Id
// @return data <no value> 信息
// @return isFound 是否找到
// @return err 错误信息
//
func (s *UserAppCmdService) QueryById(ctx context.Context, tenantId string, id string) (*query_dto.UserFindByIdResponse, bool, error) {
	var resp query_dto.UserFindByIdResponse
	isFound, err := s.BaseAppQueryService.QueryById(ctx, tenantId, id, &resp)
	if err != nil {
		return nil, false, err
	}
	return &resp, isFound, nil
}

//
// QueryByIds
// @Description: 按ids获取<no value>投影类
// @receiver s queryAppService
// @param ctx 上下文
// @param tenantId  租户id
// @param ids 多个<no value>Id
// @return data <no value> 信息
// @return isFound 是否找到
// @return err 错误信息
//
func (s *UserAppCmdService) QueryByIds(ctx context.Context, tenantId string, ids []string) (*query_dto.UserFindByIdsResponse, bool, error) {
	var resp query_dto.UserFindByIdsResponse
	isFound, err := s.BaseAppQueryService.QueryByIds(ctx, tenantId, ids, &resp)
	if err != nil {
		return nil, false, err
	}
	return &resp, isFound, nil
}
