package service

import (
	"context"
    "testing"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/user/appcmd"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/user/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestUserAppCmdService(t *testing.T) {
	ctx := context.Background()
	service := NewUserAppCmdService()

	id := randomutils.UUID()
    tenantId:= test.TenantId

	t.Run("UserCreateCommand", func(t *testing.T) {
		cmd := &appcmd.UserCreateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.UserCreateFields{
            CaseId : "CaseId",
            Email : randomutils.String(10),
            Id: id,
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "UserCreateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.UserCreate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("UserDeleteCommand", func(t *testing.T) {
		cmd := &appcmd.UserDeleteAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.UserDeleteFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "UserDeleteAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.UserDelete(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("UserUpdateCommand", func(t *testing.T) {
		cmd := &appcmd.UserUpdateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.UserUpdateFields{
            CaseId : "CaseId",
            Email : randomutils.String(10),
            Id: id,
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "UserUpdateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.UserUpdate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindAggregateById", func(t *testing.T) {
		agg, ok, err := service.FindAggregateById(ctx, tenantId, id)
		if err != nil {
			t.Error(err)
			return
		} else if ok {
            if err := test.PrintJson(t, "FindAggregateById", agg); err != nil {
                t.Error(err)
            }
		} else {
			t.Log("not found")
		}
	})
}
