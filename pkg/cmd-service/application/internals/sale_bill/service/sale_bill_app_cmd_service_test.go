package service

import (
	"context"
    "testing"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/sale_bill/appcmd"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/sale_bill/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestSaleBillAppCmdService(t *testing.T) {
	ctx := context.Background()
	service := NewSaleBillAppCmdService()

	id := randomutils.UUID()
    tenantId:= test.TenantId

	t.Run("SaleBillConfirmCommand", func(t *testing.T) {
		cmd := &appcmd.SaleBillConfirmAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleBillConfirmFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "SaleBillConfirmAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleBillConfirm(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("SaleBillCreateCommand", func(t *testing.T) {
		cmd := &appcmd.SaleBillCreateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleBillCreateFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            SaleMoney : randomutils.Float64(),
            SaleTime : randomutils.PTime(),
            TenantId : tenantId,
            UserId : "UserId",
            UserName : randomutils.NameCN(),
		}

        if err := test.PrintJson(t, "SaleBillCreateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleBillCreate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("SaleBillDeleteCommand", func(t *testing.T) {
		cmd := &appcmd.SaleBillDeleteAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleBillDeleteFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "SaleBillDeleteAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleBillDelete(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("SaleBillUpdateCommand", func(t *testing.T) {
		cmd := &appcmd.SaleBillUpdateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleBillUpdateFields{
            CaseId : "CaseId",
            Id: id,
            Remarks : randomutils.String(10),
            SaleMoney : randomutils.Float64(),
            SaleTime : randomutils.PTime(),
            TenantId : tenantId,
            UserId : "UserId",
            UserName : randomutils.NameCN(),
		}

        if err := test.PrintJson(t, "SaleBillUpdateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleBillUpdate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("SaleItemCreateCommand", func(t *testing.T) {
		cmd := &appcmd.SaleItemCreateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleItemCreateFields{
                Items:[]*field.SaleItemCreateItem{
                    &field.SaleItemCreateItem{
                        Id:id,
                        InventoryId:"InventoryId",
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                        InventoryName:randomutils.NameCN(),
                        Quantity:randomutils.Int64(),
                        Money:randomutils.Float64(),
                    },
                },
            SaleBillId : "SaleBillId",
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "SaleItemCreateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleItemCreate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("SaleItemDeleteCommand", func(t *testing.T) {
		cmd := &appcmd.SaleItemDeleteAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleItemDeleteFields{
                Items:[]*field.SaleItemDeleteItem{
                    &field.SaleItemDeleteItem{
                        Id:id,
                        TenantId:tenantId,
                        CaseId:"CaseId",
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                    },
                },
            SaleBillId : "SaleBillId",
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "SaleItemDeleteAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleItemDelete(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("SaleItemUpdateCommand", func(t *testing.T) {
		cmd := &appcmd.SaleItemUpdateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.SaleItemUpdateFields{
                Items:[]*field.SaleItemUpdateItem{
                    &field.SaleItemUpdateItem{
                        InventoryName:randomutils.NameCN(),
                        Money:randomutils.Float64(),
                        Remarks:randomutils.String(10),
                        SaleBillId:"SaleBillId",
                        Id:id,
                        InventoryId:"InventoryId",
                        Quantity:randomutils.Int64(),
                        TenantId:tenantId,
                        CaseId:"CaseId",
                    },
                },
            SaleBillId : "SaleBillId",
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "SaleItemUpdateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.SaleItemUpdate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindAggregateById", func(t *testing.T) {
		agg, ok, err := service.FindAggregateById(ctx, tenantId, id)
		if err != nil {
			t.Error(err)
			return
		} else if ok {
            if err := test.PrintJson(t, "FindAggregateById", agg); err != nil {
                t.Error(err)
            }
		} else {
			t.Log("not found")
		}
	})
}
