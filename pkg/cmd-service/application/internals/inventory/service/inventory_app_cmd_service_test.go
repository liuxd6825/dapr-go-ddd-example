package service

import (
	"context"
    "testing"
	"github.com/liuxd6825/dapr-go-ddd-sdk/utils/randomutils"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/inventory/appcmd"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/field"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
)

func TestInventoryAppCmdService(t *testing.T) {
	ctx := context.Background()
	service := NewInventoryAppCmdService()

	id := randomutils.UUID()
    tenantId:= test.TenantId

	t.Run("InventoryCreateCommand", func(t *testing.T) {
		cmd := &appcmd.InventoryCreateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.InventoryCreateFields{
            Brand : randomutils.String(10),
            CaseId : "CaseId",
            Id: id,
            Keywords : randomutils.String(10),
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            Spec : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "InventoryCreateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.InventoryCreate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("InventoryUpdateCommand", func(t *testing.T) {
		cmd := &appcmd.InventoryUpdateAppCmd{}
        cmd.CommandId = randomutils.UUID()
        cmd.IsValidOnly = false
		cmd.Data = field.InventoryUpdateFields{
            Brand : randomutils.String(10),
            CaseId : "CaseId",
            Id: id,
            Keywords : randomutils.String(10),
            Name : randomutils.NameCN(),
            Remarks : randomutils.String(10),
            Spec : randomutils.String(10),
            TenantId : tenantId,
		}

        if err := test.PrintJson(t, "InventoryUpdateAppCmd", cmd); err != nil {
            t.Error(err)
        }

		if err := service.InventoryUpdate(ctx, cmd); err != nil {
			t.Error(err)
		}
	})

	t.Run("FindAggregateById", func(t *testing.T) {
		agg, ok, err := service.FindAggregateById(ctx, tenantId, id)
		if err != nil {
			t.Error(err)
			return
		} else if ok {
            if err := test.PrintJson(t, "FindAggregateById", agg); err != nil {
                t.Error(err)
            }
		} else {
			t.Log("not found")
		}
	})
}
