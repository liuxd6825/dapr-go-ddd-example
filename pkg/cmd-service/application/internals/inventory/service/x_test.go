package service

import (
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/test"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/event"
)

func init() {
    test.InitCommand(event.GetRegisterEventTypes())
}
