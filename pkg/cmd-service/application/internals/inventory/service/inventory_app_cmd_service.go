package service

import (
	"context"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/inventory/appcmd"
    "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/application/internals/inventory/executor"
	"github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/model"
	domain_service "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/domain/inventory/service"
	query_dto "github.com/liuxu6825/dapr-go-ddd-example/pkg/query-service/userinterface/rest/inventory/dto"
	base "github.com/liuxu6825/dapr-go-ddd-example/pkg/cmd-service/infrastructure/base/application/service"
)

type InventoryAppCmdService struct {
    base.BaseAppQueryService
    inventoryCreateCommandExecutor executor.InventoryCreateCommandExecutor
    inventoryUpdateCommandExecutor executor.InventoryUpdateCommandExecutor
    findAggregateByIdExecutor executor.FindAggregateByIdExecutor
}

//
// NewInventoryAppCmdService
// @Description:  <no value>
// @return *InventoryAppCmdService
//
func NewInventoryAppCmdService() *InventoryAppCmdService {
	res := &InventoryAppCmdService{
        inventoryCreateCommandExecutor: executor.GetInventoryCreateCommandExecutor(),
        inventoryUpdateCommandExecutor: executor.GetInventoryUpdateCommandExecutor(),
        findAggregateByIdExecutor: executor.GetFindAggregateByIdExecutor(),
	}
    res.Init("ddd-example-query-service", "inventories", "v1.0")
    return res
}

//
// InventoryCreate
// @Description: 创建存货档案
// @receiver s
// @param ctx 上下文
// @param cmd 创建存货档案命令DTO对象
// @return error
//
func (s *InventoryAppCmdService) InventoryCreate(ctx context.Context, appCmd *appcmd.InventoryCreateAppCmd) error {
	return s.inventoryCreateCommandExecutor.Execute(ctx, appCmd)
}

//
// InventoryUpdate
// @Description: 更新存货档案
// @receiver s
// @param ctx 上下文
// @param cmd 更新存货档案命令DTO对象
// @return error
//
func (s *InventoryAppCmdService) InventoryUpdate(ctx context.Context, appCmd *appcmd.InventoryUpdateAppCmd) error {
	return s.inventoryUpdateCommandExecutor.Execute(ctx, appCmd)
}

//
// FindAggregateById
// @Description:
// @receiver s
// @param ctx 上下文
// @param tenantId 租户Id
// @param id 聚合根Id
// @return error
//
func (s *InventoryAppCmdService) FindAggregateById(ctx context.Context, tenantId string, id string) (*model.InventoryAggregate, bool, error) {
	return s.findAggregateByIdExecutor.Execute(ctx, tenantId, id)
}

//
// QueryById
// @Description: 按id获取<no value>投影类
// @receiver s queryAppService
// @param ctx 上下文
// @param tenantId  租户id
// @param id <no value> Id
// @return data <no value> 信息
// @return isFound 是否找到
// @return err 错误信息
//
func (s *InventoryAppCmdService) QueryById(ctx context.Context, tenantId string, id string) (*query_dto.InventoryFindByIdResponse, bool, error) {
	var resp query_dto.InventoryFindByIdResponse
	isFound, err := s.BaseAppQueryService.QueryById(ctx, tenantId, id, &resp)
	if err != nil {
		return nil, false, err
	}
	return &resp, isFound, nil
}

//
// QueryByIds
// @Description: 按ids获取<no value>投影类
// @receiver s queryAppService
// @param ctx 上下文
// @param tenantId  租户id
// @param ids 多个<no value>Id
// @return data <no value> 信息
// @return isFound 是否找到
// @return err 错误信息
//
func (s *InventoryAppCmdService) QueryByIds(ctx context.Context, tenantId string, ids []string) (*query_dto.InventoryFindByIdsResponse, bool, error) {
	var resp query_dto.InventoryFindByIdsResponse
	isFound, err := s.BaseAppQueryService.QueryByIds(ctx, tenantId, ids, &resp)
	if err != nil {
		return nil, false, err
	}
	return &resp, isFound, nil
}
